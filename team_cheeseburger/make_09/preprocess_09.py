import pandas as pd
import numpy as np

import requests

url = 'https://biendata.com/competition/airquality/bj/2017-01-01-0/2018-06-20-23/2k0d1d8'
respones= requests.get(url)
with open ("data/bj_airquality_extend.csv",'w') as f:
    f.write(respones.text)
    
url = 'https://biendata.com/competition/airquality/ld/2017-01-01-0/2018-06-20-23/2k0d1d8'
respones= requests.get(url)
with open ("data/ld_airquality_extend.csv",'w') as f:
    f.write(respones.text)


url = 'https://www.csie.ntu.edu.tw/~b03902055/kdd/bj_uvindex.csv'
respones= requests.get(url)
with open ("data/bj_uvindex.csv",'w') as f:
    f.write(respones.text)

url = 'https://www.csie.ntu.edu.tw/~b03902055/kdd/ld_uvindex.csv'
respones= requests.get(url)
with open ("data/ld_uvindex.csv",'w') as f:
    f.write(respones.text)


#meo = pd.read_csv('data/beijing_17_18_meo.csv')
#aq = pd.read_csv('data/beijing_17_18_aq.csv')
#aq23 = pd.read_csv('data/beijing_201802_201803_aq.csv')
#aqld = pd.read_csv('data/London_historical_aqi_forecast_stations_20180331.csv')

aq_bj_new = pd.read_csv('data/bj_airquality_extend.csv')
aq_ld_new = pd.read_csv('data/ld_airquality_extend.csv')

uv_bj = pd.read_csv('data/bj_uvindex.csv')
uv_ld = pd.read_csv('data/ld_uvindex.csv')

import datetime
x = datetime.datetime.now() - datetime.timedelta(hours=8)

import sys
m_now = x.month
d_now = x.day

while True:
    url = 'https://learner.csie.ntu.edu.tw/~kevinorjohn/2018-' + str(m_now).zfill(2) + '-' + str(d_now).zfill(2) + '/bj_hourly_worldweather.csv'
    respones= requests.get(url)
    if respones.ok:
        break
    x = x - datetime.timedelta(days=1)
    m_now = x.month
    d_now = x.day

with open ("data/bj_weather.csv",'w') as f:
    f.write(respones.text)
    
while True:
    url = 'https://learner.csie.ntu.edu.tw/~kevinorjohn/2018-' + str(m_now).zfill(2) + '-' + str(d_now).zfill(2) + '/ld_hourly_worldweather.csv'
    respones= requests.get(url)
    if respones.ok:
        break
    x = x - datetime.timedelta(days=1)
    m_now = x.month
    d_now = x.day


with open ("data/ld_weather.csv",'w') as f:
    f.write(respones.text)


#meo_bj = pd.read_csv('data/bj_worldweather.csv')
#meo_ld = pd.read_csv('data/ld_worldweather.csv')
meo_bj = pd.read_csv('data/bj_weather.csv')
meo_ld = pd.read_csv('data/ld_weather.csv')



import datetime
def time_diff(t2, t1):

    date=t1.split(' ')[0].split('-')
    hms=t1.split(' ')[-1].split(':')
    y1=int(date[0])
    mon1=int(date[1])
    d1=int(date[2])
    h1=int(hms[0])
    m1=int(hms[1])
    s1=int(hms[2])

    date=t2.split(' ')[0].split('-')
    hms=t2.split(' ')[-1].split(':')
    y2=int(date[0])
    mon2=int(date[1])
    d2=int(date[2])
    h2=int(hms[0])
    m2=int(hms[1])
    s2=int(hms[2])
    
    td = datetime.datetime(y1,mon1,d1,h1,m1,s1)-datetime.datetime(y2,mon2,d2,h2,m2,s2)
    return td.days*24 + td.seconds//3600

def str2dt(s):
    s = s.replace('T', ' ').replace('Z', '')
    date=s.split(' ')[0].split('-')
    hms=s.split(' ')[-1].split(':')
    y1=int(date[0])
    mon1=int(date[1])
    d1=int(date[2])
    h1=int(hms[0])
    m1=int(hms[1])
    s1=int(hms[2])

    return datetime.datetime(y1,mon1,d1,h1,m1,s1)


aq_pm25_bj = np.load('npy/aq_pm25_new.npy').item()
aq_pm10_bj = np.load('npy/aq_pm10_new.npy').item()
aq_o3_bj = np.load('npy/aq_o3_new.npy').item()

aq_stationId = np.load('npy/aq_stationId.npy').item()
datetimes_bj = np.load('npy/datetimes_new.npy')


Daq_pm25_bj = {}
Daq_pm10_bj = {}
Daq_o3_bj = {}
Dmeo_bj = {}
Duv_bj = {}


for station in aq_stationId:
    Daq_pm25_bj[station] = {}
    Daq_pm10_bj[station] = {}
    Daq_o3_bj[station] = {}
    
    Dmeo_bj[station] = {}
    Duv_bj[station] = {}
    

for station in aq_stationId:
        
    for i, aq in enumerate(aq_pm25_bj[station]):
        Daq_pm25_bj[station][datetimes_bj[i]] = aq
    for i, aq in enumerate(aq_pm10_bj[station]):
        Daq_pm10_bj[station][datetimes_bj[i]] = aq
    for i, aq in enumerate(aq_o3_bj[station]):
        Daq_o3_bj[station][datetimes_bj[i]] = aq



from datetime import timedelta, date, datetime

def daterange(datetime1, datetime2):
    for n in range(int ((datetime2 - datetime1).days + 1)*24):
        yield datetime1 + timedelta(hours=n)

start_dt = datetime(2017, 1, 1, 14, 0, 0)
end_dt = datetime(2019, 3, 31, 16, 0, 0)

datetimes_bj_new=[]
for dt in daterange(start_dt, end_dt):
    s = dt.strftime("%Y-%m-%d %H:%M:%S")
    if s.startswith('2018-06-19 13'):
        break
    datetimes_bj_new.append(dt)




import math
import datetime



for i in range(len(aq_bj_new['station_id'])):
    station = aq_bj_new['station_id'][i]
    
    if not station in aq_stationId:
        #print('not in dict: ', station)
        continue
    
    pm25 = aq_bj_new['PM25_Concentration'][i]
    pm10 = aq_bj_new['PM10_Concentration'][i]
    o3 = aq_bj_new['O3_Concentration'][i]
    utc_time = str2dt(aq_bj_new['time'][i])
    
    
    Daq_pm25_bj[station][utc_time] = pm25
    Daq_pm10_bj[station][utc_time] = pm10
    Daq_o3_bj[station][utc_time] = o3

for i in range(len(uv_bj['station'])):
    station = uv_bj['station'][i]
    
    if not (station+'_aq') in aq_stationId:
        #print('not in dict: ', station)
        continue
        
    utc_time = str2dt(uv_bj['time'][i])
    uv = uv_bj['uv_index'][i]
    
    Duv_bj[station+'_aq'][utc_time] = uv
    
    
for i in range(len(meo_bj['station_id'])):
    station = meo_bj['station_id'][i]
    
    if not station in aq_stationId:
        #print('not in dict: ', station)
        continue
    
    utc_time = str2dt(meo_bj['time'][i])
    precipMM = meo_bj['precipMM'][i]
    ws = meo_bj['windspeedKmph'][i]
    dewpoint = meo_bj['DewPointC'][i]
    feelslike = meo_bj['FeelsLikeC'][i]
    heat = meo_bj['HeatIndexC'][i]
    windchill = meo_bj['WindChillC'][i]
    windgust = meo_bj['WindGustKmph'][i]
    cloudcover = meo_bj['cloudcover'][i]
    humid = meo_bj['humidity'][i]
    pressure = meo_bj['pressure'][i]
    temp = meo_bj['tempC'][i]
    visibility = meo_bj['visibility'][i]
    winddir = meo_bj['winddirDegree'][i]

    Dmeo_bj[station][utc_time] = [precipMM, ws, dewpoint, feelslike, heat, windchill, windgust, cloudcover, humid, pressure, temp, visibility, winddir]

meo_bj_len=len(Dmeo_bj[station][utc_time])


aq_pm25_bj_new = {}
aq_pm10_bj_new = {}
aq_o3_bj_new = {}


meo_bj_new = {}
uv_bj_new = {}


for station in aq_stationId:
    aq_pm25_bj_new[station] = []
    aq_pm10_bj_new[station] = []
    aq_o3_bj_new[station] = []


    uv_bj_new[station] = []
    meo_bj_new[station] = []

for station in aq_stationId:
    for dt in datetimes_bj_new:
        if not dt in Daq_pm25_bj[station] or math.isnan(Daq_pm25_bj[station][dt]):
            Daq_pm25_bj[station][dt] = Daq_pm25_bj[station][dt-datetime.timedelta(0, 3600)]
        if not dt in Daq_pm10_bj[station] or math.isnan(Daq_pm10_bj[station][dt]):
            Daq_pm10_bj[station][dt] = Daq_pm10_bj[station][dt-datetime.timedelta(0, 3600)]
        if not dt in Duv_bj[station] or math.isnan(Duv_bj[station][dt]):
            if dt-datetime.timedelta(0, 3600) in Duv_bj[station]:
                Duv_bj[station][dt] = Duv_bj[station][dt-datetime.timedelta(0, 3600)]
            else:
                Duv_bj[station][dt] = 0

        if not dt in Daq_o3_bj[station] or math.isnan(Daq_o3_bj[station][dt]):
            Daq_o3_bj[station][dt] = Daq_o3_bj[station][dt-datetime.timedelta(0, 3600)]
         
        for i in range(meo_bj_len):
            if not dt in Dmeo_bj[station] or math.isnan(Dmeo_bj[station][dt][i]):
                Dmeo_bj[station][dt] = Dmeo_bj[station][dt-datetime.timedelta(0, 3600)]
        
        aq_pm25_bj_new[station].append(Daq_pm25_bj[station][dt])
        aq_pm10_bj_new[station].append(Daq_pm10_bj[station][dt])
        aq_o3_bj_new[station].append(Daq_o3_bj[station][dt])
        uv_bj_new[station].append(Duv_bj[station][dt])
        
        for i in range(meo_bj_len):
            if i >= len(meo_bj_new[station]):
                meo_bj_new[station].append([])
            meo_bj_new[station][i].append(Dmeo_bj[station][dt][i])
    
    


np.save('npy/aq_pm25_bj_new.npy', aq_pm25_bj_new)
np.save('npy/aq_pm10_bj_new.npy', aq_pm10_bj_new)
np.save('npy/aq_o3_bj_new.npy', aq_o3_bj_new)

np.save('npy/meo_bj_new.npy', meo_bj_new)
np.save('npy/uv_bj_new.npy', uv_bj_new)

np.save('npy/datetimes_bj_new.npy', datetimes_bj_new)



aq_pm25_ld = np.load('npy/aq_pm25_ld.npy').item()
aq_pm10_ld = np.load('npy/aq_pm10_ld.npy').item()
aq_o3_ld = np.load('npy/aq_o3_ld.npy').item()
datetimes_ld = np.load('npy/datetimes_ld.npy')
aq_stationId_ld = np.load('npy/aq_stationId_ld.npy').item()




Daq_pm25_ld = {}
Daq_pm10_ld = {}
Daq_o3_ld = {}

Dmeo_ld = {}
Duv_ld = {}

datetimes_ld = np.load('npy/datetimes_ld.npy')

for station in aq_stationId_ld:
    if not station in Daq_pm25_ld:
        Daq_pm25_ld[station] = {}
        Daq_pm10_ld[station] = {}
        Daq_o3_ld[station] = {}

        Dmeo_ld[station] = {}
        Duv_ld[station] = {}
        
    for i, aq in enumerate(aq_pm25_ld[station]):
        Daq_pm25_ld[station][datetimes_ld[i]] = aq
    for i, aq in enumerate(aq_pm10_ld[station]):
        Daq_pm10_ld[station][datetimes_ld[i]] = aq
    for i, aq in enumerate(aq_o3_ld[station]):
        Daq_o3_ld[station][datetimes_ld[i]] = aq


import math
import datetime



for i in range(len(aq_ld_new['station_id'])):
    station = aq_ld_new['station_id'][i]
    
    if not station in aq_pm25_ld:
        #print('not in dict: ', station)
        continue
    
    pm25 = aq_ld_new['PM25_Concentration'][i]
    pm10 = aq_ld_new['PM10_Concentration'][i]
    o3 = aq_ld_new['NO2_Concentration'][i]
    utc_time = str2dt(aq_ld_new['time'][i])
    
    
    Daq_pm25_ld[station][utc_time] = pm25
    Daq_pm10_ld[station][utc_time] = pm10
    Daq_o3_ld[station][utc_time] = o3
    

for i in range(len(uv_ld['station'])):
    station = uv_ld['station'][i]
    
    if not (station+'_aq') in aq_stationId_ld:
        #print('not in dict: ', station)
        continue
        
    utc_time = str2dt(uv_ld['time'][i])
    uv = uv_ld['uv_index'][i]
    
    Duv_ld[station+'_aq'][utc_time] = uv  

for i in range(len(meo_ld['station_id'])):
    station = meo_ld['station_id'][i]
    
    if not station in aq_pm25_ld:
        #print('not in dict: ', station)
        continue
    
    utc_time = str2dt(meo_ld['time'][i])
    precipMM = meo_ld['precipMM'][i]
    ws = meo_ld['windspeedKmph'][i]
    
    dewpoint = meo_ld['DewPointC'][i]
    feelslike = meo_ld['FeelsLikeC'][i]
    heat = meo_ld['HeatIndexC'][i]
    windchill = meo_ld['WindChillC'][i]
    windgust = meo_ld['WindGustKmph'][i]
    cloudcover = meo_ld['cloudcover'][i]
    humid = meo_ld['humidity'][i]
    pressure = meo_ld['pressure'][i]
    temp = meo_ld['tempC'][i]
    visibility = meo_ld['visibility'][i]
    winddir = meo_ld['winddirDegree'][i]

    Dmeo_ld[station][utc_time] = [precipMM, ws, dewpoint, feelslike, heat, windchill, windgust, cloudcover, humid, pressure, temp, visibility, winddir]

meo_ld_len=len(Dmeo_ld[station][utc_time])


from datetime import timedelta, date, datetime

def daterange(datetime1, datetime2):
    for n in range(int ((datetime2 - datetime1).days + 1)*24):
        yield datetime1 + timedelta(hours=n)

start_dt = datetimes_ld[0]
end_dt = datetime(2019, 3, 31, 16, 0, 0)

datetimes_ld_new=[]
for dt in daterange(start_dt, end_dt):
    s = dt.strftime("%Y-%m-%d %H:%M:%S")
    if s.startswith('2018-06-22 00'):
        break
    datetimes_ld_new.append(dt)

import datetime


aq_pm25_ld_new = {}
aq_pm10_ld_new = {}
aq_o3_ld_new = {}

uv_ld_new = {}
meo_ld_new = {}

for station in aq_stationId_ld:
    aq_pm25_ld_new[station] = []
    aq_pm10_ld_new[station] = []
    aq_o3_ld_new[station] = []

    uv_ld_new[station] = []
    meo_ld_new[station] = []

for station in aq_stationId_ld:
    for dt in datetimes_ld_new:
        if not dt in Daq_pm25_ld[station] or math.isnan(Daq_pm25_ld[station][dt]):
            Daq_pm25_ld[station][dt] = Daq_pm25_ld[station][dt-datetime.timedelta(0, 3600)]
        if not dt in Daq_pm10_ld[station] or math.isnan(Daq_pm10_ld[station][dt]):
            Daq_pm10_ld[station][dt] = Daq_pm10_ld[station][dt-datetime.timedelta(0, 3600)]
        if not dt in Daq_o3_ld[station] or math.isnan(Daq_o3_ld[station][dt]):
            Daq_o3_ld[station][dt] = Daq_o3_ld[station][dt-datetime.timedelta(0, 3600)]
            
        if not dt in Duv_ld[station] or math.isnan(Duv_ld[station][dt]):
            if dt-datetime.timedelta(0, 3600) in Duv_ld[station]:
                Duv_ld[station][dt] = Duv_ld[station][dt-datetime.timedelta(0, 3600)]
            else:
                Duv_ld[station][dt] = 0
            
            
            
        for i in range(meo_ld_len):
            if not dt in Dmeo_ld[station] or math.isnan(Dmeo_ld[station][dt][i]):
                Dmeo_ld[station][dt] = Dmeo_ld[station][dt-datetime.timedelta(0, 3600)]
        
    
        
        aq_pm25_ld_new[station].append(Daq_pm25_ld[station][dt])
        aq_pm10_ld_new[station].append(Daq_pm10_ld[station][dt])
        aq_o3_ld_new[station].append(Daq_o3_ld[station][dt])
        
        uv_ld_new[station].append(Duv_ld[station][dt])
        
        for i in range(meo_ld_len):
            if i >= len(meo_ld_new[station]):
                meo_ld_new[station].append([])
            meo_ld_new[station][i].append(Dmeo_ld[station][dt][i])
            
    
    


np.save('npy/aq_pm25_ld_new.npy', aq_pm25_ld_new)
np.save('npy/aq_pm10_ld_new.npy', aq_pm10_ld_new)
np.save('npy/aq_o3_ld_new.npy', aq_o3_ld_new)

np.save('npy/uv_ld_new.npy', uv_ld_new)
np.save('npy/meo_ld_new.npy', meo_ld_new)


np.save('npy/datetimes_ld_new.npy', datetimes_ld_new)



