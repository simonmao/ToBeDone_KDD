rm -f ./data/beijing_aq_extend.csv
rm -f ./data/london_aq_extend.csv
cp -f ./data/beijing_aq_full.csv ./data/beijing_aq_extend.csv
cp -f ./data/London_historical_aqi_forecast_stations_20180331.csv ./data/london_aq_extend.csv
python ./fill_data_everyday.py --download_latest
cat ./bj_tmp.csv >> ./data/beijing_aq_extend.csv
cat ./ld_tmp.csv >> ./data/london_aq_extend.csv

python3 ./fill_data_everyday.py --create_csv
