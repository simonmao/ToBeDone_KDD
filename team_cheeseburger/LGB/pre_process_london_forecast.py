import numpy as np
np.random.seed(233)
import pandas as pd
import sys
import operator
import datetime
import pickle
import codecs
#sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())

from collections import Counter

data_dir = '/tmp/Jhopo/kdd_data/'
#data_dir = '/tmp2/Jhopo/kdd_data/'
aqi_london_csv = data_dir + 'ld_knn_interp.csv'
#aqi_other_csv = data_dir + 'London_historical_aqi_other_stations_20180331.csv'
location_txt = data_dir + 'london_station_location.txt'

forecast_csv = data_dir + 'forecast/ld_hourly_worldweather.csv'

meo_grid_csv = data_dir + 'London_historical_meo_grid.csv'

london_id_list = ['MY7', 'KF1', 'CD1', 'TH4' ,'CD9', 'GR4', 'GR9', 'ST5', 'LW2', 'HV1', 'GN3', 'BL0', 'GN0']
#other_id_list = ['HR1', 'RB7', 'TD5', 'CR8', 'BX9', 'GB0', 'CT3', 'CT2', 'KC1', 'LH0', 'BX1']


def log_smoothing(x):
    if x >= 0:
        return np.log1p(x)
    else:
        return np.log1p(0)


def read_ld_location_txt(location_txt):
    loc_dict = {}
    with open(location_txt, 'r') as f:
        for line in f:
            aq_id = line.rstrip().split(' ')[0]
            x, y = float(line.rstrip().split(' ')[1]), float(line.rstrip().split(' ')[2])
            loc_dict[aq_id] = (x, y)

    ld_neighbors = {}
    for aq_id in forecast_id_list:
        xi, yi = loc_dict[aq_id]

        neighbors = {}
        for neighbor in forecast_id_list:
            if aq_id != neighbor:
                xn, yn = loc_dict[neighbor]
                neighbors[neighbor] = np.square(xi-xn) + np.square(yi-yn)

        sorted_neighbors = sorted(neighbors.items(), key=operator.itemgetter(1))
        neighbors_list = [neighbor[0] for neighbor in sorted_neighbors]
        ld_neighbors[aq_id] = neighbors_list

    return ld_neighbors


def read_ld_aq_csv(aqi_data_csv, location_id_list):
    aqi_data = pd.read_csv(aqi_data_csv)

    aqi_id = aqi_data["station_id"].values
    aqi_time = aqi_data["time"].values
    aqi_pm25 = aqi_data["PM25_Concentration"].values
    aqi_pm10 = aqi_data["PM10_Concentration"].values

    # build dict for all
    all_data = {}
    for target_id in location_id_list:
        dict_pollut = {}
        for idx, l_id in enumerate(aqi_id):
            if l_id == target_id:
                now_t = datetime.datetime.strptime(aqi_time[idx], '%Y-%m-%d %H:%M:%S')
                dict_pollut[now_t] = [float(aqi_pm25[idx]), float(aqi_pm10[idx])]

        all_data[target_id] = dict_pollut

    return all_data


def read_ld_forecast_csv(forecast_csv):
    fc_data = pd.read_csv(forecast_csv)

    fc_id = fc_data["station_id"].fillna("MissingID").values
    fc_time = fc_data["time"].values
    fc_windspeed = fc_data["windspeedKmph"].fillna(-1).values
    fc_temp = fc_data["tempF"].fillna(-1).values
    fc_cloud = fc_data["cloudcover"].fillna(-1).values
    fc_winddir = fc_data["winddirDegree"].fillna(-1).values
    fc_precip = fc_data["precipMM"].fillna(-1).values
    fc_windgust = fc_data["WindGustKmph"].fillna(-1).values


    forecast_all = (fc_id, fc_time, fc_windspeed, fc_temp, fc_cloud, fc_winddir, fc_precip, fc_windgust)
    return forecast_all


def get_ld_aq_data(predict_date, target_id, all_data, forecast_all, label_name, is_forecast):
    one_day = datetime.timedelta(days=1)
    end_all = (predict_date - 2*one_day).strftime("%Y/%m/%d")
    end_valid = (predict_date - 5*one_day).strftime("%Y/%m/%d")
    end_test = (predict_date - 3*one_day).strftime("%Y/%m/%d")


    fc_id, fc_time, fc_windspeed, fc_temp, fc_cloud, fc_winddir, fc_precip, fc_windgust = forecast_all
    def create_forecast_dict(fc_wea):
        dict_wea = {}
        for idx, l_id in enumerate(fc_id):
            if l_id == target_id:
                now_t = datetime.datetime.strptime(fc_time[idx], '%Y-%m-%d %H:%M:%S')
                dict_wea[now_t] = fc_wea[idx]

        return dict_wea

    if is_forecast == True:
        dict_windspeed = create_forecast_dict(fc_windspeed)
        dict_temp = create_forecast_dict(fc_temp)
        dict_cloud = create_forecast_dict(fc_cloud)
        dict_winddir = create_forecast_dict(fc_winddir)
        dict_precip = create_forecast_dict(fc_precip)
        dict_windgust = create_forecast_dict(fc_windgust)


    def collect_data(begin_time, end_time, all_data, label_name, is_label=True):
        now_t = datetime.datetime.strptime(begin_time, '%Y/%m/%d %H:%M')
        end_t = datetime.datetime.strptime(end_time, '%Y/%m/%d %H:%M')
        one_hour = datetime.timedelta(hours=1)
        one_day = datetime.timedelta(days=1)

        # create training and testing data
        # X : 48 hours
        # y : 48 hour
        X = []
        y_48 = [[] for _ in range(48)]
        t_time = []

        while True:
            # air pollution
            X_t = now_t - 48*one_hour
            X_day = []
            for _ in range(48):
                X_day += all_data[target_id][X_t]
                X_t += one_hour


            # waether forecast
            if is_forecast == True:
                X_t = now_t
                for _ in range(48):
                    X_day.append(dict_windspeed[X_t])
                    #X_day.append(dict_temp[X_t])
                    #X_day.append(dict_cloud[X_t])
                    X_day.append(dict_winddir[X_t])
                    #X_day.append(dict_precip[X_t])
                    X_day.append(dict_windgust[X_t])
                    X_t += one_hour

            X.append(X_day)

            t_time.append(now_t)

            if is_label:
                y_t = now_t
                for hour_idx in range(48):
                    if label_name == 'pm25':
                        y_48[hour_idx].append(all_data[target_id][y_t][0])
                    if label_name == 'pm10':
                        y_48[hour_idx].append(all_data[target_id][y_t][1])
                    y_t += one_hour

            now_t += one_day
            if now_t > end_t:
                break

        return X, y_48, t_time

    X_train, y_train, t_train = [], [], []
    X_valid, y_valid, t_valid = [], [], []
    X_test, y_test, t_test = [], [], []
    X, y, y_time = collect_data('2017/01/03 00:00', '2018/03/30 23:00', all_data, label_name, is_label=True)
    X_train += X
    y_train += y
    t_train += y_time

    X, y, y_time = collect_data('2018/04/01 00:00', end_valid+' 23:00', all_data, label_name, is_label=True)
    X_valid += X
    y_valid += y
    t_valid += y_time

    X, y, y_time = collect_data('2018/04/01 00:00', end_test+' 23:00', all_data, label_name, is_label=False)
    X_test += X
    t_test += y_time


    X_train, y_train, t_train = np.array(X_train), np.array(y_train), np.array(t_train)
    X_valid, y_valid, t_valid = np.array(X_valid), np.array(y_valid), np.array(t_valid)
    X_test, t_test = np.array(X_test), np.array(t_test)

    print (X_train.shape, y_train.shape, t_train.shape)
    print (X_valid.shape, y_valid.shape, t_valid.shape)
    print (X_test.shape, t_test.shape)

    return X_train, y_train, t_train, X_valid, y_valid, t_valid, X_test, t_test


def get_ld_aq_data_daily(predict_date, target_id, all_data, forecast_all, label_name, is_forecast):
    one_day = datetime.timedelta(days=1)
    begin_test = predict_date.strftime("%Y/%m/%d")
    end_test = predict_date.strftime("%Y/%m/%d")


    fc_id, fc_time, fc_windspeed, fc_temp, fc_cloud, fc_winddir, fc_precip, fc_windgust = forecast_all
    def create_forecast_dict(fc_wea):
        dict_wea = {}
        for idx, l_id in enumerate(fc_id):
            if l_id == target_id:
                now_t = datetime.datetime.strptime(fc_time[idx], '%Y-%m-%d %H:%M:%S')
                dict_wea[now_t] = fc_wea[idx]

        return dict_wea

    if is_forecast == True:
        dict_windspeed = create_forecast_dict(fc_windspeed)
        dict_temp = create_forecast_dict(fc_temp)
        dict_cloud = create_forecast_dict(fc_cloud)
        dict_winddir = create_forecast_dict(fc_winddir)
        dict_precip = create_forecast_dict(fc_precip)
        dict_windgust = create_forecast_dict(fc_windgust)


    def collect_data(begin_time, end_time, all_data, label_name, is_label=True):
        now_t = datetime.datetime.strptime(begin_time, '%Y/%m/%d %H:%M')
        end_t = datetime.datetime.strptime(end_time, '%Y/%m/%d %H:%M')
        one_hour = datetime.timedelta(hours=1)
        one_day = datetime.timedelta(days=1)

        # create training and testing data
        # X : 48 hours
        # y : 48 hour
        X = []
        y_48 = [[] for _ in range(48)]
        t_time = []

        while True:
            # air pollution
            X_t = now_t - 48*one_hour
            X_day = []
            for _ in range(48):
                X_day += all_data[target_id][X_t]
                X_t += one_hour


            # waether forecast
            if is_forecast == True:
                X_t = now_t
                for _ in range(48):
                    X_day.append(dict_windspeed[X_t])
                    #X_day.append(dict_temp[X_t])
                    #X_day.append(dict_cloud[X_t])
                    X_day.append(dict_winddir[X_t])
                    #X_day.append(dict_precip[X_t])
                    X_day.append(dict_windgust[X_t])
                    X_t += one_hour

            X.append(X_day)

            t_time.append(now_t)

            now_t += one_day
            if now_t > end_t:
                break

        return X, y_48, t_time

    X_test, y_test, t_test = [], [], []

    X, y, y_time = collect_data(begin_test+' 00:00', end_test+' 23:00', all_data, label_name, is_label=False)
    X_test += X
    t_test += y_time

    X_test, t_test = np.array(X_test), np.array(t_test)

    print (X_test.shape, t_test.shape)

    return X_test, t_test


def read_ld_meo_csv(meo_grid_csv):
    meo_data = pd.read_csv(meo_grid_csv)

    meo_id = meo_data["stationName"].fillna("MissingID").values
    meo_time = meo_data["utc_time"].values
    meo_longitude = meo_data["longitude"].values
    meo_latitude = meo_data["latitude"].values
    meo_temperature = meo_data["temperature"].fillna(-1).values
    meo_pressure = meo_data["pressure"].fillna(-1).values
    meo_humidity = meo_data["humidity"].fillna(-1).values
    meo_direction = meo_data["wind_direction"].fillna(-1).values
    meo_speed = meo_data["wind_speed/kph"].fillna(-1).values

    # Normalize longitude and latitude
    meo_longitude = list(map(lambda x: int(x*10+20), meo_longitude))
    meo_latitude = list(map(lambda x: int(x*10-505), meo_latitude))

    # Remove 999017 in wind_direction (no need in London data)
    #meo_direction = [x if x != 999017 else -1 for x in meo_direction]


    return meo_id, meo_time, meo_longitude, meo_latitude, meo_temperature, meo_pressure, meo_humidity, meo_direction, meo_speed


def append_ld_meo_data(X, begin_time, end_time, meo_id, meo_time, meo_longitude, meo_latitude, meo_temperature, meo_pressure, meo_humidity, meo_direction, meo_speed):
    dict_meo = {}
    for idx, t in enumerate(meo_time):
        now_t = datetime.datetime.strptime(t, '%Y-%m-%d %H:%M:%S')
        feat = [meo_temperature[idx], meo_pressure[idx], meo_humidity[idx], meo_direction[idx], meo_speed[idx]]

        if now_t not in dict_meo:
            dict_meo[now_t] = feat
        else:
            dict_meo[now_t] += feat


    now_t = datetime.datetime.strptime(begin_time, '%Y/%m/%d %H:%M')
    end_t = datetime.datetime.strptime(end_time, '%Y/%m/%d %H:%M')
    one_hour = datetime.timedelta(hours=1)

    idx = 0
    new_X = []
    while True:
        X_t = now_t - 6*one_hour
        X_day = X[idx].tolist()
        for _ in range(6):
            X_day += dict_meo[now_t]
        new_X.append(X_day)
        idx += 1

        now_t += one_hour
        if now_t > end_t:
            break

    return np.array(new_X)



def get_ld_meo_data(meo_id, meo_time, meo_longitude, meo_latitude, meo_temperature, meo_pressure, meo_humidity, meo_direction, meo_speed):
    try:
        with open('./data/London_meo.pkl', 'rb') as f:
            pkl = pickle.load(f)
            X_train = pkl['X_train']
            y_train = pkl['y_train']
            X_test = pkl['X_test']
            y_test = pkl['y_test']

    except:
        dict_meo = {}
        for idx, t in enumerate(meo_time):
            now_t = datetime.datetime.strptime(t, '%Y-%m-%d %H:%M:%S')
            feat = [meo_temperature[idx], meo_pressure[idx], meo_humidity[idx], meo_direction[idx], meo_speed[idx]]

            if now_t not in dict_meo:
                dict_meo[now_t] = [feat]
            else:
                dict_meo[now_t].append(feat)


        now_t = datetime.datetime.strptime('2017-01-01 00:00:00', '%Y-%m-%d %H:%M:%S')
        end_t = datetime.datetime.strptime('2018-03-26 23:00:00', '%Y-%m-%d %H:%M:%S')
        one_hour = datetime.timedelta(hours=1)

        meo_all = []
        while True:
            meo_all.append(np.array(dict_meo[now_t]).reshape((41,21,5)))

            now_t += one_hour
            if now_t > end_t:
                break

        meo_all = np.array(meo_all)

        X_all = meo_all[:-1]
        y_all = meo_all[1:]

        X_train, y_train = X_all[:8760], y_all[:8760]
        X_test, y_test = X_all[8760:], y_all[8760:]

        print (X_train.shape, y_train.shape)
        print (X_test.shape, y_test.shape)

        data = {'X_train': X_train, 'y_train': y_train, 'X_test': X_test, 'y_test': y_test}

        with open('./data/London_meo.pkl', 'wb') as f:
            pickle.dump(data, f)

    return X_train, y_train, X_test, y_test



if __name__ == '__main__':
    predict_date = datetime.datetime.strptime('2018-05-22 00:00:00', '%Y-%m-%d %H:%M:%S')

    ld_data = read_ld_aq_csv(aqi_london_csv, london_id_list)
    forecast_all = read_ld_forecast_csv(forecast_csv)

    target_id = 'CD1'
    X_train, y_train, t_train, X_valid, y_valid, t_valid, X_test, t_test = get_ld_aq_data(predict_date, target_id, ld_data, forecast_all, 'pm25', is_forecast=True)


    #meo_id, meo_time, meo_longitude, meo_latitude, meo_temperature, meo_pressure, meo_humidity, meo_direction, meo_speed = read_meo_csv(meo_grid_csv)
    #X_train, y_train, X_test, y_test = get_meo_data(meo_id, meo_time, meo_longitude, meo_latitude, meo_temperature, meo_pressure, meo_humidity, meo_direction, meo_speed)
