import requests
import numpy as np
np.random.seed(233)
import pandas as pd
import sys
import datetime
import pickle


beijing_id_list = ['yongdingmennei_aq', 'dingling_aq', 'shunyi_aq', 'dongsi_aq', 'fangshan_aq', 'miyun_aq', 'fengtaihuayuan_aq', 'donggaocun_aq', 'nansanhuan_aq', 'yanqin_aq', 'yongledian_aq', 'yizhuang_aq', 'pinggu_aq', 'xizhimenbei_aq', 'tongzhou_aq', 'dongsihuan_aq', 'yufa_aq', 'gucheng_aq', 'badaling_aq', 'zhiwuyuan_aq', 'daxing_aq', 'tiantan_aq', 'yungang_aq', 'beibuxinqu_aq', 'liulihe_aq', 'wanliu_aq', 'miyunshuiku_aq', 'huairou_aq', 'wanshouxigong_aq', 'pingchang_aq', 'mentougou_aq', 'nongzhanguan_aq', 'guanyuan_aq', 'qianmen_aq', 'aotizhongxin_aq']

forecast_id_list = ['MY7', 'KF1', 'CD1', 'TH4' ,'CD9', 'GR4', 'GR9', 'ST5', 'LW2', 'HV1', 'GN3', 'BL0', 'GN0']


def donwload_data_with_api(city, data, begin_time, end_time):
    url = 'https://biendata.com/competition/{}/{}/{}/{}/2k0d1d8'.format(data, city, begin_time, end_time)
    respones = requests.get(url)
    file_name = '{}_{}_today.csv'.format(city, data)
    with open (file_name,'w') as f:
        f.write(respones.text)


def get_beijing_aq_with_api(file_name, neighbors, target_id, begin_time, end_time):
    aqi_data = pd.read_csv(file_name)

    aqi_id = aqi_data["station_id"].fillna("MissingID").values
    aqi_time = aqi_data["time"].values
    aqi_pm25 = aqi_data["PM25_Concentration"].fillna(0.0001).values
    aqi_pm10 = aqi_data["PM10_Concentration"].fillna(0.0001).values
    aqi_o3 = aqi_data["O3_Concentration"].fillna(0.0001).values

    # build dict for all
    all_data = {}
    for target_id in beijing_id_list:
        dict_pollut = {}
        for idx, l_id in enumerate(aqi_id):
            if l_id == target_id:
                now_t = aqi_time[idx]
                if now_t not in dict_pollut:
                    dict_pollut[now_t] = (aqi_pm25[idx], aqi_pm10[idx], aqi_o3[idx])
        all_data[target_id] = dict_pollut

    def get_5_neighbor(l_id, now_t, pollut_idx):
        nei_value = []
        for nei_idx, neighbor in enumerate(neighbors[l_id]):
            if now_t in all_data[neighbor]:
                if all_data[neighbor][now_t][pollut_idx] != 0.0001:
                    nei_value.append(all_data[neighbor][now_t][pollut_idx])

            if nei_idx >= 2:
                break

        if len(nei_value) >= 1:
            return np.mean(nei_value)
        else:
            return 0.0001

    # fill missing value with top 5 nearest neighbors
    for idx, l_id in enumerate(aqi_id):
        if aqi_pm25[idx] == 0.0001:
            aqi_pm25[idx] = get_5_neighbor(l_id, aqi_time[idx], 0)
        if aqi_pm10[idx] == 0.0001:
            aqi_pm10[idx] = get_5_neighbor(l_id, aqi_time[idx], 1)
        if aqi_o3[idx] == 0.0001:
            aqi_o3[idx] = get_5_neighbor(l_id, aqi_time[idx], 2)


    def create_dict(aqi_pollut):
        dict_pollut = {}
        for idx, l_id in enumerate(aqi_id):
            if l_id == target_id:
                now_t = datetime.datetime.strptime(aqi_time[idx], '%Y-%m-%d %H:%M:%S')

                if now_t not in dict_pollut:
                    dict_pollut[now_t] = [aqi_pollut[idx]]
                else:
                    dict_pollut[now_t].append(aqi_pollut[idx])

        # make duplicated entries become average of them
        for t in dict_pollut:
            if len(dict_pollut[t]) == 1:
                dict_pollut[t] = dict_pollut[t][0]
            elif len(dict_pollut[t]) > 1:
                dict_pollut[t] = np.mean(dict_pollut[t])

        # fill empty time entry with previous time entry
        one_hour = datetime.timedelta(hours=1)
        one_day = datetime.timedelta(days=1)
        now_t = datetime.datetime.strptime(begin_time, '%Y-%m-%d-%H') - 2*one_day
        end_t = datetime.datetime.strptime(end_time, '%Y-%m-%d-%H')


        while True:
            if now_t not in dict_pollut:
                dict_pollut[now_t] = dict_pollut[now_t-one_hour]

            now_t += one_hour
            if now_t > end_t:
                break


        # fill -1 with previous time entry
        now_t = datetime.datetime.strptime(begin_time, '%Y-%m-%d-%H') - 2*one_day
        end_t = datetime.datetime.strptime(end_time, '%Y-%m-%d-%H')

        while True:
            if dict_pollut[now_t] == 0.0001:
                dict_pollut[now_t] = dict_pollut[now_t-one_hour]

            now_t += one_hour
            if now_t > end_t:
                break

        return dict_pollut

    dict_pm25 = create_dict(aqi_pm25)
    dict_pm10 = create_dict(aqi_pm10)
    dict_o3 = create_dict(aqi_o3)

    one_hour = datetime.timedelta(hours=1)
    one_day = datetime.timedelta(days=1)
    now_t = datetime.datetime.strptime(begin_time, '%Y-%m-%d-%H')
    end_t = datetime.datetime.strptime(end_time, '%Y-%m-%d-%H')


    X_test = []
    t_test = []

    while True:
        X_t = now_t - 48*one_hour
        X_day = []
        for _ in range(48):
            X_day.append(dict_pm25[X_t])
            X_day.append(dict_pm10[X_t])
            X_day.append(dict_o3[X_t])
            X_t += one_hour
        X_test.append(X_day)

        t_test.append(now_t)

        now_t += one_day
        if now_t > end_t:
            break

    X_test, t_test = np.array(X_test), np.array(t_test)

    print (X_test.shape, t_test.shape)

    return X_test, t_test


def get_london_aq_with_api(file_name, neighbors, target_id, begin_time, end_time):
    aqi_data = pd.read_csv(file_name)

    aqi_id = aqi_data["station_id"].fillna("MissingID").values
    aqi_time = aqi_data["time"].values
    aqi_pm25 = aqi_data["PM25_Concentration"].fillna(-1).values
    aqi_pm10 = aqi_data["PM10_Concentration"].fillna(-1).values
    aqi_no2 = aqi_data["NO2_Concentration"].fillna(-1).values

    # build dict for all
    all_data = {}
    for target_id in forecast_id_list:
        dict_pollut = {}
        for idx, l_id in enumerate(aqi_id):
            if l_id == target_id:
                now_t = aqi_time[idx]
                if now_t not in dict_pollut:
                    dict_pollut[now_t] = (aqi_pm25[idx], aqi_pm10[idx], aqi_no2[idx])
        all_data[target_id] = dict_pollut

    def get_5_neighbor(l_id, now_t, pollut_idx):
        nei_value = []
        for nei_idx, neighbor in enumerate(neighbors[l_id]):
            if now_t in all_data[neighbor]:
                if all_data[neighbor][now_t][pollut_idx] != 0.0001:
                    nei_value.append(all_data[neighbor][now_t][pollut_idx])

            if nei_idx >= 2:
                break

        if len(nei_value) >= 1:
            return np.mean(nei_value)
        else:
            return 0.0001

    # fill missing value with top 5 nearest neighbors
    for idx, l_id in enumerate(aqi_id):
        if l_id in forecast_id_list:
            if aqi_pm25[idx] == 0.0001:
                aqi_pm25[idx] = get_5_neighbor(l_id, aqi_time[idx], 0)
            if aqi_pm10[idx] == 0.0001:
                aqi_pm10[idx] = get_5_neighbor(l_id, aqi_time[idx], 1)
            if aqi_no2[idx] == 0.0001:
                aqi_no2[idx] = get_5_neighbor(l_id, aqi_time[idx], 2)


    def create_dict(aqi_pollut):
        dict_pollut = {}
        for idx, l_id in enumerate(aqi_id):
            if l_id == target_id:
                now_t = datetime.datetime.strptime(aqi_time[idx], '%Y-%m-%d %H:%M:%S')

                if now_t not in dict_pollut:
                    dict_pollut[now_t] = [aqi_pollut[idx]]
                else:
                    dict_pollut[now_t].append(aqi_pollut[idx])

        # make duplicated entries become average of them
        for t in dict_pollut:
            if len(dict_pollut[t]) == 1:
                dict_pollut[t] = dict_pollut[t][0]
            elif len(dict_pollut[t]) > 1:
                dict_pollut[t] = np.mean(dict_pollut[t])

        # fill empty time entry with previous time entry
        one_hour = datetime.timedelta(hours=1)
        one_day = datetime.timedelta(days=1)
        now_t = datetime.datetime.strptime(begin_time, '%Y-%m-%d-%H') - 2*one_day
        end_t = datetime.datetime.strptime(end_time, '%Y-%m-%d-%H')


        while True:
            if now_t not in dict_pollut:
                dict_pollut[now_t] = dict_pollut[now_t-one_hour]

            now_t += one_hour
            if now_t > end_t:
                break


        # fill -1 with previous time entry
        now_t = datetime.datetime.strptime(begin_time, '%Y-%m-%d-%H') - 2*one_day
        end_t = datetime.datetime.strptime(end_time, '%Y-%m-%d-%H')

        while True:
            if dict_pollut[now_t] == -1:
                dict_pollut[now_t] = dict_pollut[now_t-one_hour]

            now_t += one_hour
            if now_t > end_t:
                break

        return dict_pollut

    dict_pm25 = create_dict(aqi_pm25)
    dict_pm10 = create_dict(aqi_pm10)
    dict_no2 = create_dict(aqi_no2)

    one_hour = datetime.timedelta(hours=1)
    one_day = datetime.timedelta(days=1)
    now_t = datetime.datetime.strptime(begin_time, '%Y-%m-%d-%H')
    end_t = datetime.datetime.strptime(end_time, '%Y-%m-%d-%H')


    X_test = []
    t_test = []

    while True:
        X_t = now_t - 48*one_hour
        X_day = []
        for _ in range(48):
            X_day.append(dict_pm25[X_t])
            X_day.append(dict_pm10[X_t])
            X_day.append(dict_no2[X_t])
            X_t += one_hour
        X_test.append(X_day)

        t_test.append(now_t)

        now_t += one_day
        if now_t > end_t:
            break

    X_test, t_test = np.array(X_test), np.array(t_test)

    print (X_test.shape, t_test.shape)

    return X_test, t_test


def download_latest_data(begin_time='2017-01-01-0', end_time='2018-07-31-0'):
    city, data, begin_time, end_time = 'bj', 'airquality', begin_time, end_time
    donwload_data_with_api(city, data, begin_time, end_time)

    city, data, begin_time, end_time = 'ld', 'airquality', begin_time, end_time
    donwload_data_with_api(city, data, begin_time, end_time)


def append_latest_data():
    output_fd = open('./bj_tmp.csv', 'w')

    with open('./bj_airquality_today.csv', 'r') as f:
        f.readline()
        while True:
            line = f.readline()
            if not line: break
            line_list = line.rstrip().split(',')
            #t = datetime.datetime.strptime(line_list[2], '%Y-%m-%d %H:%M:%S')
            #if t >= datetime.datetime.strptime('2018-03-29 00:00:00', '%Y-%m-%d %H:%M:%S') and t <= datetime.datetime.strptime('2018-03-31 06:00:00', '%Y-%m-%d %H:%M:%S'):
            output_fd.write('{},'.format(line_list[1]))
            output_fd.write('{},'.format(line_list[2]))
            #output_fd.write('{},'.format(datetime.datetime.strptime(line_list[2], '%Y-%m-%d %H:%M:%S').strftime("%Y/%m/%d %H:%M")))
            output_fd.write('{},'.format(line_list[3]))
            output_fd.write('{},'.format(line_list[4]))
            output_fd.write('{},'.format(line_list[5]))
            output_fd.write('{},'.format(line_list[6]))
            output_fd.write('{},'.format(line_list[7]))
            output_fd.write('{}\n'.format(line_list[8]))


    output_fd = open('./ld_tmp.csv', 'w')

    with open('./ld_airquality_today.csv', 'r') as f:
        f.readline()
        while True:
            line = f.readline()
            if not line: break
            line_list = line.rstrip().split(',')
            #t = datetime.datetime.strptime(line_list[2], '%Y-%m-%d %H:%M:%S')
            #if t >= datetime.datetime.strptime('2018-03-29 00:00:00', '%Y-%m-%d %H:%M:%S') and t <= datetime.datetime.strptime('2018-03-31 06:00:00', '%Y-%m-%d %H:%M:%S'):
            output_fd.write('{},'.format(line_list[0]))

            output_fd.write('{},'.format(datetime.datetime.strptime(line_list[2], '%Y-%m-%d %H:%M:%S').strftime("%Y/%m/%d %H:%M")))

            output_fd.write('{},'.format(line_list[1]))

            output_fd.write('{},'.format(line_list[3]))
            output_fd.write('{},'.format(line_list[4]))
            output_fd.write('{}\n'.format(line_list[5]))


if __name__ == '__main__':
    download_latest_data(begin_time='2017-01-01-0', end_time='2018-07-31-0')
    append_latest_data()


    '''
    city, data, begin_time, end_time = 'bj', 'airquality', '2018-04-25-0', '2018-04-28-23'
    donwload_data_with_api(city, data, begin_time, end_time)

    #target_id = 'yongdingmennei_aq'
    #X_test, t_test = get_beijing_aq_with_api('./data/bj_airquality_2018-03-30-0-2018-04-18-23.csv', target_id, begin_time, end_time)


    city, data, begin_time, end_time = 'ld', 'airquality', '2018-04-25-0', '2018-04-28-23'
    donwload_data_with_api(city, data, begin_time, end_time)
    #target_id = 'CD1'
    #X_test, t_test = get_london_aq_with_api('./data/ld_airquality_2018-03-31-0-2018-04-18-23.csv', target_id, begin_time, end_time)
    '''
