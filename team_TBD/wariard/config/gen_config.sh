for i in $(seq 2 12)
do
    cp ./$1/dataset_$2_1.yml ./$1/dataset_$2_$i.yml
done

sed -i "" 's/prev_1h-6h/prev_1h-12h/g' ./$1/dataset_$2_2.yml
sed -i "" 's/prev_1h-6h/prev_1h-18h/g' ./$1/dataset_$2_3.yml
sed -i "" 's/prev_1h-6h/prev_1h-24h/g' ./$1/dataset_$2_4.yml
sed -i "" 's/prev_1h-6h/prev_1h-30h/g' ./$1/dataset_$2_5.yml
sed -i "" 's/prev_1h-6h/prev_1h-36h/g' ./$1/dataset_$2_6.yml
sed -i "" 's/prev_1h-6h/prev_1h-42h/g' ./$1/dataset_$2_7.yml
sed -i "" 's/prev_1h-6h/prev_1h-48h/g' ./$1/dataset_$2_8.yml
sed -i "" 's/prev_1h-6h/prev_1h-54h/g' ./$1/dataset_$2_9.yml
sed -i "" 's/prev_1h-6h/prev_1h-60h/g' ./$1/dataset_$2_10.yml
sed -i "" 's/prev_1h-6h/prev_1h-66h/g' ./$1/dataset_$2_11.yml
sed -i "" 's/prev_1h-6h/prev_1h-72h/g' ./$1/dataset_$2_12.yml
