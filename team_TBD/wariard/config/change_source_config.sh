cp ./.feagenrc/source_config/$1_valid.yml ./.feagenrc/source_config/$2_valid.yml
sed -i "" 's/'$1'/'$2'/g' ./.feagenrc/source_config/$2_valid.yml

cp ./.feagenrc/source_config/$1.yml ./.feagenrc/source_config/$2.yml
sed -i "" 's/'$1'/'$2'/g' ./.feagenrc/source_config/$2.yml

sed -i "" 's/'$1'/'$2'/g' ./gen_feagen.sh

mkdir -p ./.feagenrc/april_dataset_config/$2/