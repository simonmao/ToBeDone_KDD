import pandas as pd
import numpy as np
import os
import logging
import argparse
import json

from sheet_writer import SheetWriter

bj_station_list = [
        'aotizhongxin_aq', 'badaling_aq', 'beibuxinqu_aq', 'daxing_aq',
       'dingling_aq', 'donggaocun_aq', 'dongsi_aq', 'dongsihuan_aq',
       'fangshan_aq', 'fengtaihuayuan_aq', 'guanyuan_aq', 'gucheng_aq',
       'huairou_aq', 'liulihe_aq', 'mentougou_aq', 'miyun_aq',
       'miyunshuiku_aq', 'nansanhuan_aq', 'nongzhanguan_aq',
       'pingchang_aq', 'pinggu_aq', 'qianmen_aq', 'shunyi_aq',
       'tiantan_aq', 'tongzhou_aq', 'wanliu_aq', 'wanshouxigong_aq',
       'xizhimenbei_aq', 'yanqin_aq', 'yizhuang_aq', 'yongdingmennei_aq',
       'yongledian_aq', 'yufa_aq', 'yungang_aq', 'zhiwuyuan_aq',]

ld_station_list = ['CD1', 'BL0', 'GR4', 'MY7', 'HV1', 'GN3', 'GR9', 'LW2',
                'GN0', 'KF1', 'CD9', 'ST5', 'TH4']

def symmetric_mean_absolute_percentage_error(actual, predicted):

    actual = actual.ravel()
    predicted = predicted.ravel()

    mask = ~ pd.isnull(actual)
    actual = actual[mask]
    predicted = predicted[mask]

    a = np.abs(np.array(actual) - np.array(predicted))
    b = np.array(actual) + np.array(predicted)

    return 2 * np.mean(np.divide(a, b, out=np.zeros_like(a), where=b!=0, casting='unsafe'))

def get_logger():
    logging.basicConfig(format='%(asctime)s [%(levelname)s] [%(filename)s:%(lineno)s - %(funcName)s() ] %(message)s')
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    return logger

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--submit_date', type=str)
    parser.add_argument('--submit_path', required=True, type=str)
    parser.add_argument('--aq_dir', type=str, default='/home/guest/wariard/KDD/main_challenge/data/crawl_data/')
    parser.add_argument('--today', type=str, default='2018-05-08-17')
    parser.add_argument('--auth_json_path', type=str,
            default="/home/guest/wariard/KDD/key/auth.json")
    args = parser.parse_args()
    print(json.dumps(vars(args), indent=2, sort_keys=True))
    return args

args = parse_args()
logger = get_logger()

writer = SheetWriter(args.auth_json_path)
# writer.write_dataframe(df, sheet_type, sheet_name)
# sheet_type :  'model' for testing different model
#               'feature' for testing different feature
#               'dataset' for testing different dataset
#               'feature_importances' for observing feature importances

##### load bj and ld aq file
ld_aq_file = 'ld_aq_{}.csv'.format(args.today)
ld_aq_path = os.path.join(args.aq_dir, ld_aq_file)
ld_aq_df = pd.read_csv(ld_aq_path, parse_dates=['MeasurementDateGMT'])
ld_aq_df = ld_aq_df.rename(columns={'MeasurementDateGMT': 'time'})
ld_aq_df = ld_aq_df[ld_aq_df['station_id'].isin(ld_station_list)]

bj_aq_file = 'bj_aq_{}.csv'.format(args.today)
bj_aq_path = os.path.join(args.aq_dir, bj_aq_file)
bj_aq_df = pd.read_csv(bj_aq_path, parse_dates=['utc_time'])
bj_aq_df['stationId'] = bj_aq_df['stationId'].apply(lambda x: x.replace('_aq','')[:10] + '_aq' if 'aq' in x else x)
bj_aq_df = bj_aq_df.rename(columns={'utc_time': 'time', 'stationId': 'station_id'})

##### load pred file
pred_df = pd.read_csv(args.submit_path)
if 'submit_date' not in pred_df:
    pred_df['submit_date'] = args.submit_date
    logger.info("Need submit date sinice no submite_date column")
pred_df = pred_df.set_index(['submit_date', 'test_id'])
ans_df = pd.DataFrame(index=pred_df.index, columns=pred_df.columns)


##### compute smape
task = {'bj': ['PM2.5', 'PM10', 'O3'], 'ld': ['PM2.5', 'PM10']  }
submit_dates_str = pred_df.index.get_level_values('submit_date').unique()
for submit_date_str in submit_dates_str:

    submit_date = pd.to_datetime(submit_date_str)
    start_date = submit_date + pd.offsets.Day(1)#'2018-03-19 00:00:00'
    end_date = submit_date + pd.offsets.Day(2) + pd.offsets.Hour(23) #'2018-03-20 23:00:00'

    for city in list(task.keys()):

        pollutants = task[city]

        aq_df = bj_aq_df if city == 'bj' else ld_aq_df

        for station_id in aq_df['station_id'].unique():

            ##### get prediction dataframe index
            station_filter = pred_df.index.get_level_values('test_id').str.contains(station_id)
            date_filter = pred_df.index.get_level_values('submit_date') == submit_date_str
            pred_df_temp = pred_df[ station_filter & date_filter ]

            ##### get ans dataframe values
            station_filter = aq_df['station_id'] == station_id
            date_filter = (aq_df['time'] >= start_date) & (aq_df['time'] <= end_date)
            ans_df_temp = aq_df[ station_filter & date_filter ]

            ## assign ans values based on prediction index
            ans_df.loc[pred_df_temp.index, pollutants] = ans_df_temp[pollutants].values


#### SMAPE code from TA

pd_merged_all = pd.merge( ans_df.reset_index(), pred_df.reset_index(), how='left', on=['submit_date','test_id'] )

# Data preprocessing: Split test_id
pd_merged_all['stat_id'], pd_merged_all['hr'] = pd_merged_all['test_id'].str.split('#', 1).str

# Data preprocessing: Replace negative values with np.nan
#pd_merged_all[ pd_merged_all < 0 ] = np.nan

# Data preprocessing: Remove missing value in gt
pd_merged_lon = pd_merged_all.loc[ pd_merged_all['stat_id'].isin(ld_station_list) ]
pd_merged_bj = pd_merged_all.loc[ ~pd_merged_all['stat_id'].isin(ld_station_list) ]

pd_merged_lon = pd_merged_lon.replace('', np.nan, regex=True).dropna( subset=['PM2.5_x', 'PM10_x'], how='any' )
pd_merged_bj = pd_merged_bj.replace('', np.nan, regex=True).dropna( subset=['PM2.5_x', 'PM10_x', 'O3_x'], how='any' )

pd_merged_all = pd.concat( [pd_merged_lon, pd_merged_bj] )

for submit_date_str in pd_merged_all['submit_date'].unique():

    pd_merged_bj_temp = pd_merged_bj[pd_merged_bj['submit_date'] == submit_date_str]
    pd_merged_lon_temp = pd_merged_lon[pd_merged_lon['submit_date'] == submit_date_str]

    bj_SMAPE = symmetric_mean_absolute_percentage_error(pd_merged_bj_temp[['PM2.5_x','PM10_x','O3_x']].values,
                                                        pd_merged_bj_temp[['PM2.5_y','PM10_y','O3_y']].values)
    bj_PM25_SMAPE = symmetric_mean_absolute_percentage_error(pd_merged_bj_temp[['PM2.5_x']].values,
                                                        pd_merged_bj_temp[['PM2.5_y']].values)
    bj_PM10_SMAPE = symmetric_mean_absolute_percentage_error(pd_merged_bj_temp[['PM10_x']].values,
                                                        pd_merged_bj_temp[['PM10_y']].values)
    bj_O3_SMAPE = symmetric_mean_absolute_percentage_error(pd_merged_bj_temp[['O3_x']].values,
                                                        pd_merged_bj_temp[['O3_y']].values)
    lon_SMAPE = symmetric_mean_absolute_percentage_error(pd_merged_lon_temp[['PM2.5_x','PM10_x']].values,
                                                        pd_merged_lon_temp[['PM2.5_y','PM10_y']].values)
    lon_PM25_SMAPE = symmetric_mean_absolute_percentage_error(pd_merged_lon_temp[['PM2.5_x']].values,
                                                        pd_merged_lon_temp[['PM2.5_y']].values)
    lon_PM10_SMAPE = symmetric_mean_absolute_percentage_error(pd_merged_lon_temp[['PM10_x']].values,
                                                        pd_merged_lon_temp[['PM10_y']].values)

    pd_merged = pd_merged_all[pd_merged_all['submit_date'] == submit_date_str]
    new_SMAPE = symmetric_mean_absolute_percentage_error(pd_merged[['PM2.5_x','PM10_x','O3_x']].values,
                                                  pd_merged[['PM2.5_y','PM10_y','O3_y']].values)

    date_filter = ans_df.index.get_level_values('submit_date') == submit_date_str
    old_SMAPE = symmetric_mean_absolute_percentage_error(ans_df[date_filter].values,
                                                        pred_df[date_filter].values)
    logger.info("{} - old : {}, new : {}".format(submit_date.strftime("%Y-%m-%d"),
               old_SMAPE, new_SMAPE))

    ###### feature index
    tuples = [( submit_date_str, os.path.basename(args.submit_path)) ]
    task_index = pd.MultiIndex.from_tuples(tuples, names=[
         'submit_date', 'filename'])

    SMAPE_df = pd.DataFrame(data={'old SMAPE':[old_SMAPE],
                                  'new SMAPE':[new_SMAPE],
                                  'bj SMAPE':[bj_SMAPE],
                                  'lon SMAPE':[lon_SMAPE],
                                  'official SMAPE': [(bj_SMAPE+lon_SMAPE)/2],
                                  'bj_PM25':[bj_PM25_SMAPE],
                                  'bj_PM10':[bj_PM10_SMAPE],
                                  'bj_O3':[bj_O3_SMAPE],
                                  'lon_PM25':[lon_PM25_SMAPE],
                                  'lon_PM10':[lon_PM10_SMAPE]}, index=task_index)

    writer.write_dataframe(df=SMAPE_df[['official SMAPE', 'bj SMAPE', 'lon SMAPE',
                                        'bj_PM25', 'bj_PM10', 'bj_O3',
                                        'lon_PM25', 'lon_PM10']], sheet_type='smape',
                            sheet_name='detail', TITLE_FLAG=False)
    writer.write_dataframe(df=SMAPE_df[[ 'bj SMAPE', 'lon SMAPE', 'official SMAPE']], sheet_type='smape',
                            sheet_name='simple', TITLE_FLAG=False)
