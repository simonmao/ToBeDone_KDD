import pandas as pd
import sys

source_1 = pd.read_csv("/tmp2/TBD/test/test_"+sys.argv[1]+"_01.csv")
source_2 = pd.read_csv("/tmp2/TBD/test/test_"+sys.argv[1]+"_02.csv")

source_3 = pd.merge(source_1, source_2, on=['test_id'])

for label in ['PM2.5', 'PM10', 'O3']:
    source_3[label] = (source_3[label+"_x"] + source_3[label+"_y"]) / 2

source_3.to_csv("/tmp2/TBD/ensemble/test_"+sys.argv[1]+"_ensemble.csv", index=False, columns=['test_id', 'PM2.5', 'PM10', 'O3'])