import pandas as pd
import numpy as np
from datetime import datetime
from datetime import timedelta
import ipdb

import sys

def clip(data_panda):
    for label in ['PM2.5', 'PM10', 'O3']:
        data_panda[label] = np.where(data_panda[label] < 0, 1.0, data_panda[label])
    return data_panda

def minus(data_panda, label, offset, hour_start):
    data_panda[label] = np.where(data_panda['hour'] >= hour_start, data_panda[label]-offset, data_panda[label])
    return data_panda

def set_time(data_panda):
    data_utc = data_panda[["submit_date", "test_id", "PM2.5", "PM10", "O3", "hour", "station_id"]].values
    utc_list = []
    for i in range(len(data_utc)):
        utc_list.append(datetime.strptime(str(data_utc[i][0]), "%Y-%m-%d")+timedelta(days=1, hours=data_utc[i][5]))
    data_panda['time'] = utc_list
    return data_panda

def get_analysis(label, all_data):
    all_data[label+"_diff"] = all_data[label] - all_data[label+"_ans"]
    all_data[label+"_ratio"] = all_data[label+"_ans"] / all_data[label]
    return all_data

def rain_info(all_data):
    station_list = np.unique(all_data["station_id"].values)
    station_panda_list = []
    for station in station_list:
        station_panda = all_data[all_data["station_id"] == station]
        station_panda_value = station_panda[["time", "precipMM"]].values
        rain_duration_list = []
        after_max = []
        stop_rain_list = []
        rain_duration = 0
        max_rain = 0
        stop_rain = 0
        for index in range(len(station_panda_value)):
            current_rain = float(station_panda_value[index][1])
            rain_duration_list.append(rain_duration)
            if current_rain < max_rain:
                after_max.append(1)
                if current_rain == 0.0:
                    max_rain = 0.0
            else:
                after_max.append(0)
                max_rain = current_rain

            if current_rain > 0.0:
                rain_duration += 1
                stop_rain = 0
            else:
                rain_duration = 0
                stop_rain += 1
            stop_rain_list.append(stop_rain)

        station_panda["rain_duration"] = rain_duration_list
        station_panda["after_max"] = after_max
        station_panda["stop_rain"] = stop_rain_list
        station_panda_list.append(station_panda)

    return pd.concat(station_panda_list)



data_panda = pd.read_csv(sys.argv[1])
bj_world = pd.read_csv(sys.argv[2])[["station_id", "time", "precipMM"]]
bj_world["time"] = pd.to_datetime(bj_world["time"])
bj_world["station_id"] = bj_world["station_id"].apply(lambda x:x.replace('_aq', '')[:10] + "_aq" if 'aq' in x else x)

data_panda['hour'] = [int(test_id.split('#')[1]) for test_id in data_panda['test_id']]
data_panda['station_id'] = [test_id.split('#')[0] for test_id in data_panda['test_id']]
data_panda = set_time(data_panda)
data_panda["time"] = pd.to_datetime(data_panda["time"])

station_list = ['CD1', 'BL0', 'GR4', 'MY7', 'HV1', 'GN3', 'GR9', 'LW2', 'GN0', 'KF1', 'CD9', 'ST5', 'TH4'] 

beijing_panda = data_panda[~data_panda['station_id'].isin(station_list)]
rain_beijing_panda = pd.merge(beijing_panda, bj_world, on=["station_id", "time"])


rain_beijing_panda["time"] = pd.to_datetime(rain_beijing_panda["time"])
bj_ans = pd.read_csv("~/data/05-19/bj_aq_05-19.csv")
bj_ans = bj_ans.rename(columns={"stationId": "station_id", 
                                "utc_time": "time", 
                                "PM2.5": "PM2.5_ans",
                                "PM10": "PM10_ans",
                                "O3": "O3_ans"})
bj_ans['station_id'] = bj_ans['station_id'].apply(lambda x: x.replace('_aq','')[:10] + '_aq' if 'aq' in x else x)
bj_ans["time"] = pd.to_datetime(bj_ans["time"])

all_data = pd.merge(rain_beijing_panda, bj_ans, on=["station_id", "time"])
all_data = get_analysis("PM2.5", all_data)
all_data = get_analysis("PM10", all_data)
all_data = get_analysis("O3", all_data)

all_data = rain_info(all_data)

all_data.to_csv(sys.argv[4]+"_PM2.5.csv", index=False, columns=["station_id", "time", "hour",
                                                                "precipMM", "rain_duration", 
                                                                "after_max", "stop_rain",
                                                                "PM2.5", "PM2.5_ans", "PM2.5_ratio", "PM2.5_diff"])

all_data.to_csv(sys.argv[4]+"_PM10.csv", index=False, columns=["station_id", "time", "hour",
                                                                "precipMM", "rain_duration", 
                                                                "after_max", "stop_rain",
                                                                "PM10", "PM10_ans", "PM10_ratio", "PM10_diff"])

all_data.to_csv(sys.argv[4]+"_O3.csv", index=False, columns=["station_id", "time", "hour",
                                                                "precipMM", "rain_duration", 
                                                                "after_max", "stop_rain",
                                                                "O3", "O3_ans", "O3_ratio", "O3_diff"])

#ipdb.set_trace()

"""
rain_beijing_panda = rain_filter(rain_beijing_panda)
london_panda = data_panda[data_panda['station_id'].isin(station_list)]
rain_london_panda = pd.merge(london_panda, ld_world, on=["station_id", "time"])
rain_beijing_panda = rain_beijing_panda[["submit_date", "test_id", "PM2.5", "PM10", "O3"]]
london_panda = london_panda[["submit_date", "test_id", "PM2.5", "PM10", "O3"]]
data_panda = pd.concat([rain_beijing_panda, london_panda])
data_panda.to_csv(sys.argv[4], index=False, columns=["submit_date", "test_id", "PM2.5", "PM10", "O3"])
"""