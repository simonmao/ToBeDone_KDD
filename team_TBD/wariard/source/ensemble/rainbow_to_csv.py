
# coding: utf-8

# In[72]:


import os
import json
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sys

def main(today_date):
    bj_stations = ['aotizhongxin_aq', 'badaling_aq', 'beibuxinqu_aq', 'daxing_aq', 
                   'dingling_aq', 'donggaocun_aq', 'dongsi_aq', 'dongsihuan_aq', 
                   'fangshan_aq', 'fengtaihuayuan_aq', 'guanyuan_aq', 'gucheng_aq', 
                   'huairou_aq', 'liulihe_aq', 'mentougou_aq', 'miyun_aq', 
                   'miyunshuiku_aq', 'nansanhuan_aq', 'nongzhanguan_aq', 
                   'pingchang_aq', 'pinggu_aq', 'qianmen_aq', 'shunyi_aq', 
                   'tiantan_aq', 'tongzhou_aq', 'wanliu_aq', 'wanshouxigong_aq', 
                   'xizhimenbei_aq', 'yanqin_aq', 'yizhuang_aq', 'yongdingmennei_aq', 
                   'yongledian_aq', 'yufa_aq', 'yungang_aq', 'zhiwuyuan_aq']
    ld_stations = ['BL0', 'CD1', 'CD9', 'GN0', 'GN3', 'GR4', 
                   'GR9', 'HV1', 'KF1', 'LW2', 'MY7', 'ST5', 'TH4']
    df = pd.DataFrame(columns=['test_id','PM2.5','PM10','O3'])
    for name in bj_stations:
        with open('/tmp2/TBD/external_data/rainbow/%s-23/%s-23_%s'%(today_date, today_date, name),'r') as f:
            d = f.read()
            d = json.loads(d)
            d = json.loads(d)
            d = d['result']['hourly']['pm25']
            d = pd.DataFrame(d).iloc[1:]
            d = d.append(d.iloc[-1],ignore_index=True)
            d['test_id'] = pd.Series(np.arange(48))
            d['test_id'] = d['test_id'].apply(lambda x:name.replace('_aq','')[:10]+'_aq#'+str(x))
            d['PM2.5'] = d['value']
            d['PM10'] = 0
            d['O3'] = 0
            d = d[['test_id','PM2.5','PM10','O3']]
            df = df.append(d,ignore_index=True)

    for name in ld_stations:
        with open('/tmp2/TBD/external_data/rainbow/%s-23/%s-23_%s'%(today_date, today_date, name),'r') as f:
            d = f.read()
            d = json.loads(d)
            d = json.loads(d)
            d = d['result']['hourly']['pm25']
            d = pd.DataFrame(d).iloc[1:]
            d = d.append(d.iloc[-1],ignore_index=True)
            d['test_id'] = pd.Series(np.arange(48))
            d['test_id'] = d['test_id'].apply(lambda x:name+'#'+str(x))
            d['PM2.5'] = d['value']
            d['PM10'] = 0
            d['O3'] = 0
            d = d[['test_id','PM2.5','PM10','O3']]
            df = df.append(d,ignore_index=True)
    df['submit_date'] = today_date
    df = df[['submit_date','test_id','PM2.5','PM10','O3']]
    df.to_csv('/tmp2/TBD/test/test_%s_21.csv' %(today_date[5:]),index=False)


# In[73]:


if __name__ == '__main__':
    main(sys.argv[1])


# In[74]:


#main('2018-05-13','.')


# In[ ]:




