#!/bin/bash
export PATH="$HOME/.pyenv/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"
pyenv activate venv3.6.2

eval_date=$1 #05-01
#eval_date=$(date -d "-3 day" "+%m-%d" -u) #05-01
#today=$(date "+%Y-%m-%d-%H" -u) # 2018-05-05-15

##### crawl aq data
#cd ../cronjob
#python2 crawl_raw_data.py --end_time $today --city bj --aq aq &
#python2 crawl_raw_data.py --end_time $today --city ld --aq aq &
#wait

##### compute smape for test and ensemble file
#cd ../source

#test_dir="/home/guest/wariard/KDD/main_challenge/submission/all_test"
#for file in `ls $test_dir`
#do
#    if echo $file | grep -Eq "test_"$eval_date ; then
#          echo "$file"
#          python3 compuate_smape.py --submit_date 2018-$eval_date --submit_path $test_dir/$file 
#    fi
#done

ensemble_dir="/home/guest/wariard/KDD/main_challenge/submission/all_ensemble"
for file in `ls $ensemble_dir`
do
    if echo $file | grep -Eq "ens_"$eval_date ; then
          echo "$file"
          python3 compuate_smape.py --submit_date 2018-$eval_date --submit_path $ensemble_dir/$file 
    fi
done
pyenv deactivate