import pandas as pd
import numpy as np
from datetime import datetime
from datetime import timedelta
from info import *
from sklearn.neighbors import KNeighborsRegressor
from sklearn.preprocessing import normalize
from sklearn.tree import ExtraTreeRegressor
from sklearn.ensemble import GradientBoostingRegressor
import ipdb

import sys

def set_time(data_panda):
    data_utc = data_panda[["submit_date", "test_id", "PM2.5", "PM10", "O3", "hour", "station_id"]].values
    utc_list = []
    for i in range(len(data_utc)):
        utc_list.append(datetime.strptime(str(data_utc[i][0]), "%Y-%m-%d")+timedelta(days=1, hours=data_utc[i][5]))
    data_panda.loc[:, 'time'] = utc_list
    return data_panda

def append_rain_info(all_data):
    station_list = np.unique(all_data["station_id"].values)
    station_panda_list = []
    for station in station_list:
        station_panda = all_data[all_data["station_id"] == station]
        station_panda_value = station_panda[["time", "precipMM"]].values
        rain_duration_list = []
        after_max = []
        stop_rain_list = []
        rain_list = []
        avg_rain_list = []
        max_rain_list = []
        rain_duration = 0
        max_rain = 0
        past_max_rain = 0
        stop_rain = 0
        start = True
        for index in range(len(station_panda_value)):
            current_rain = float(station_panda_value[index][1])
            rain_duration_list.append(rain_duration)

            if current_rain > 0.0:
                rain_duration += 1
                rain_list.append(current_rain)
                stop_rain = 0
                start = False
            else:
                rain_duration = 0
                rain_list = []
                stop_rain += 1
                if start or stop_rain >= 10:
                    stop_rain = 0
            stop_rain_list.append(stop_rain)

            if current_rain < max_rain and rain_duration > 0:
                after_max.append(1)
                if current_rain == 0.0:
                    max_rain = 0.0
            else:
                after_max.append(0)
                max_rain = current_rain

            if max_rain != 0:
                past_max_rain = max_rain
            if rain_duration == 0 and stop_rain == 0:
                past_max_rain = 0
            max_rain_list.append(past_max_rain)

            if rain_duration > 0 and len(rain_list) > 0:
                avg_rain_list.append(np.average(np.asarray(rain_list)))
            else:
                avg_rain_list.append(0)

        station_panda.loc[:, "rain_duration"] = rain_duration_list
        station_panda.loc[:, "after_max"] = after_max
        station_panda.loc[:, "stop_rain"] = stop_rain_list
        station_panda.loc[:, "avg_precipMM"] = avg_rain_list
        station_panda.loc[:, "max_precipMM"] = max_rain_list
        station_panda_list.append(station_panda)

    return pd.concat(station_panda_list)

def make_training_data(bj_data, label):
    print("make_training_data: "+str(label))
    train_x = []
    train_y = []
    for i in range(1, 19):
        if i < 10:
            datestr = "0"+str(i)
        else:
            datestr = str(i)

        pred = pd.read_csv("/tmp2/TBD/reproduce/test/test_05-"+str(datestr)+"_61.csv")
        pred.loc[:, 'hour'] = [int(test_id.split('#')[1]) for test_id in pred['test_id']]
        pred.loc[:, 'station_id'] = [test_id.split('#')[0] for test_id in pred['test_id']]
        pred = set_time(pred)
        pred.loc[:, "time"] = pd.to_datetime(pred["time"])
        pred.loc[:, "day"] = np.where(pred["hour"] < 23, 1, 2)
        pred.loc[:, "hour"] = np.where(pred["hour"] < 23, pred["hour"], pred["hour"]-24)

        pred_data = pd.merge(pred, bj_data, on=["station_id", "time"])
        pred_data.loc[:, label+"_ratio"] = pred_data[label+"_ans"] / pred_data[label]
        label_pred_data = pred_data[["station_id", "time", "day", "hour", label,
                                    "precipMM", "rain_duration", "after_max", "stop_rain",
                                    "avg_precipMM", "max_precipMM", label+"_ratio"]]

        name_pair = []
        for _, name in enumerate(bj_aq_station_dic):
            if len(name.replace('_aq','')) > 10:
                name_pair.append([name.replace('_aq', '')[:10] + '_aq', name])
        for name in name_pair:
            bj_aq_station_dic[name[0]] = bj_aq_station_dic[name[1]]

        round_data = label_pred_data.values
        for index in range(len(round_data)):
            if np.isnan(round_data[index][-1]):
                continue
            if round_data[index][5] != 0 or round_data[index][6] != 0 or round_data[index][8] != 0:
                if bj_aq_station_dic[round_data[index][0]]["type"] == "urban":
                    train_x.append(round_data[index][2:-1].tolist() + [1, 0, 0, 0])
                elif bj_aq_station_dic[round_data[index][0]]["type"] == "suburban":
                    train_x.append(round_data[index][2:-1].tolist() + [0, 1, 0, 0])
                elif bj_aq_station_dic[round_data[index][0]]["type"] == "traffic":
                    train_x.append(round_data[index][2:-1].tolist() + [0, 0, 1, 0])
                elif bj_aq_station_dic[round_data[index][0]]["type"] == "other":
                    train_x.append(round_data[index][2:-1].tolist() + [0, 0, 0, 1])
                train_y.append(round_data[index][-1])
    
    print(len(train_x))
    train_x = normalize(np.asarray(train_x), norm='l2')
    #model = KNeighborsRegressor(n_neighbors=2)
    #model = ExtraTreeRegressor(max_depth=4)
    '''
    if label == "PM2.5":
        model = GradientBoostingRegressor(n_estimators=50, max_depth=6, random_state=5314)
    elif label == "PM10":
        model = GradientBoostingRegressor(n_estimators=100, max_depth=6, random_state=5314)
    elif label == "O3":
        model = GradientBoostingRegressor(n_estimators=100, max_depth=8, random_state=5314)
    '''
    model = GradientBoostingRegressor(n_estimators=100, max_depth=8, random_state=5314)
    model.fit(np.asarray(train_x), np.asarray(train_y))

    return model

def make_pred_data(bj_pred, bj_world, model, label):
    print("make_pred_data: "+str(label))
    bj_pred = set_time(bj_pred)
    bj_pred.loc[:, "time"] = pd.to_datetime(bj_pred["time"])
    bj_pred.loc[:, "day"] = np.where(bj_pred["hour"] < 23, 1, 2)
    bj_pred.loc[:, "hour"] = np.where(bj_pred["hour"] < 23, bj_pred["hour"], bj_pred["hour"]-24)
    #ipdb.set_trace()
    bj_pred = pd.merge(bj_pred, bj_world, on=["station_id", "time"])
    label_pred_data = bj_pred[["station_id", "time", "day", "hour", label,
                               "precipMM", "rain_duration", "after_max", "stop_rain",
                               "avg_precipMM", "max_precipMM"]]

    name_pair = []
    for _, name in enumerate(bj_aq_station_dic):
        if len(name.replace('_aq','')) > 10:
            name_pair.append([name.replace('_aq', '')[:10] + '_aq', name])
    for name in name_pair:
        bj_aq_station_dic[name[0]] = bj_aq_station_dic[name[1]]

    round_data = label_pred_data.values
    test_x = []
    for index in range(len(round_data)):
        if bj_aq_station_dic[round_data[index][0]]["type"] == "urban":
            test_x.append(round_data[index][2:].tolist() + [1, 0, 0, 0])
        elif bj_aq_station_dic[round_data[index][0]]["type"] == "suburban":
            test_x.append(round_data[index][2:].tolist() + [0, 1, 0, 0])
        elif bj_aq_station_dic[round_data[index][0]]["type"] == "traffic":
            test_x.append(round_data[index][2:].tolist() + [0, 0, 1, 0])
        elif bj_aq_station_dic[round_data[index][0]]["type"] == "other":
            test_x.append(round_data[index][2:].tolist() + [0, 0, 0, 1])

    test_x = normalize(np.asarray(test_x), norm='l2')
    test_y = model.predict(test_x)

    for index in range(len(test_y)):
        if round_data[index][5] == 0 and round_data[index][6] == 0 and round_data[index][8] == 0:
            test_y[index] = 1.0
        elif test_y[index] < 0:
            test_y[index] = 0

    bj_pred.loc[:, label+"_ratio"] = test_y
    bj_pred.loc[:, label] = bj_pred[label] * bj_pred[label+"_ratio"]
    return bj_pred[["submit_date", "test_id", "PM2.5", "PM10", "O3", "station_id", "hour"]]

if __name__ == '__main__':
    bj_ans = pd.read_csv(sys.argv[1])
    bj_world = pd.read_csv(sys.argv[2])

    bj_ans = bj_ans.rename(columns = {"stationId": "station_id", 
                                      "utc_time": "time", 
                                      "PM2.5": "PM2.5_ans", 
                                      "PM10": "PM10_ans", 
                                      "O3": "O3_ans"})
    
    bj_ans.loc[:, "time"] = pd.to_datetime(bj_ans["time"])
    bj_ans.loc[:, 'station_id'] = bj_ans['station_id'].apply(lambda x: x.replace('_aq','')[:10] + '_aq' if 'aq' in x else x)

    bj_world.loc[:, "time"] = pd.to_datetime(bj_world["time"])
    bj_world.loc[:, 'station_id'] = bj_world['station_id'].apply(lambda x: x.replace('_aq','')[:10] + '_aq' if 'aq' in x else x)
    bj_world = bj_world[["station_id", "time", "precipMM"]]

    bj_world = bj_world[bj_world["time"] > datetime(2018, 4, 20)]
    bj_ans = bj_ans[bj_ans["time"] > datetime(2018, 4, 20)]

    bj_world = append_rain_info(bj_world)

    bj_data = pd.merge(bj_world, bj_ans, on=["station_id", "time"])
    PM25_model = make_training_data(bj_data, "PM2.5")
    PM10_model = make_training_data(bj_data, "PM10")
    O3_model = make_training_data(bj_data, "O3")

    pred = pd.read_csv(sys.argv[3])
    station_list = ['CD1', 'BL0', 'GR4', 'MY7', 'HV1', 'GN3', 'GR9', 'LW2', 'GN0', 'KF1', 'CD9', 'ST5', 'TH4'] 
    pred.loc[:, 'hour'] = [int(test_id.split('#')[1]) for test_id in pred['test_id']]
    pred.loc[:, 'station_id'] = [test_id.split('#')[0] for test_id in pred['test_id']]

    bj_pred = pred[~pred["station_id"].isin(station_list)]
    bj_pred = make_pred_data(bj_pred, bj_data, PM25_model, "PM2.5")
    bj_pred = make_pred_data(bj_pred, bj_data, PM10_model, "PM10")
    bj_pred = make_pred_data(bj_pred, bj_data, O3_model, "O3")

    ld_pred = pred[pred["station_id"].isin(station_list)]

    pred = pd.concat([bj_pred, ld_pred])
    pred.to_csv(sys.argv[4], index=False, columns=["submit_date", "test_id", "PM2.5", "PM10", "O3"])    