import pandas as pd
import time

import gspread
from oauth2client.service_account import ServiceAccountCredentials

from gspread_dataframe import get_as_dataframe, set_with_dataframe

from utils import get_logger

logger = get_logger()

class SheetWriter:

    def __init__(self, auth_json_path):
    
        # auth_json_path = '/nfs/Frigga/b03902009/drive-auth.json'
        self.auth_json_path = auth_json_path
        self.sheet_key_dict = {
                "model" : '1tnQF-fNKW6YhnllxKtCfPOpyqqtJ1uXZBjLuSu8_p8I',
                "data": '13hCP3W90MVhUzCT-inL5W7GnbZdifN_9VhXQTvDAbmc',
                "log": '1XGQ7wN06HM8LQFiMeEIfSxBBZdOhdL6EkeSktN1ydFg',
                "smape": '1wRwr5Xik8HkxPG-3UoySebMsJcJGz7kVATUxCDwoifI',

                ## simon
                "feature" : '1BWVGFAMN3QgTx5MF8DxMH92Ws7Qg3ZQFXQII8rEe2CE',
                "feature_importances": '11PowOao9VvDBPhJ721Wz4LTLVnhEnvAPxque3FNDNK4',

                ## kevinorjohn
                # "feature": '1_nUm7n9QayrpDQMfXzGr7g1Sq6a4K-O1fi9ShptOl9Y',
                # "feature_importances": '1DeZqwNagrwq4hOV2t1rzZjj-0iO_zLtW3TgU5ejMNUc',
                }

    def build_connection(self):
        
        gss_scopes = ['https://spreadsheets.google.com/feeds']
        credentials = ServiceAccountCredentials.from_json_keyfile_name(self.auth_json_path, gss_scopes)
        gss_client = gspread.authorize(credentials)

        return gss_client

    def write_dataframe(self, df, sheet_type, sheet_name, TITLE_FLAG=True):

        logger.info("Writing Dataframe")
        logger.info("Sheet type : {}".format(sheet_type))
        logger.info("Sheet name : {}".format(sheet_name))
        
        sheet_key = self.sheet_key_dict[sheet_type]
        gss_client = self.build_connection()
        sheet = gss_client.open_by_key(sheet_key)
        worksheet = sheet.worksheet(sheet_name)

        # set_with_dataframe(worksheet, df)

        count = 0
        df = df.reset_index()
        if TITLE_FLAG:
            worksheet.append_row(df.columns.astype(str).tolist())
        for idx, row in df.iterrows():
            try:
                worksheet.append_row(list(row.values))
                count += 1
                if count % 80 == 0: time.sleep(100)
            except:
                logger.info("Fail to push log to sheet")


