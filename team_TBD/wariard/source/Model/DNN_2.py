# for reproducibility
import random
random.seed(5314)
import numpy as np
np.random.seed(5314)
import tensorflow as tf
tf.set_random_seed(5314)

#sys
import logging
logging.basicConfig(format='%(asctime)s [%(levelname)s] [%(filename)s:%(lineno)s - %(funcName)s() ] %(message)s')
logger = logging.getLogger()
logger.setLevel(logging.INFO)

#data
import pandas as pd

#model
from smape_callback import Smape_callback
from keras.callbacks import EarlyStopping
from keras.models import Model
from keras.layers import Input, Dense, Embedding, SpatialDropout1D, concatenate
from keras.layers.normalization import BatchNormalization
from keras.layers import Activation
from keras.optimizers import Adam

class DNN:
    def __init__(self,
                 feature,
                 target_panda,
                 is_train,
                 is_subvalid,
                 is_valid,
                 loss,
                 batch_size=32,
                 epochs=10):

        self.feature = feature
        self.target_panda = target_panda
        self.is_train = is_train
        self.is_subvalid = is_subvalid
        self.is_valid = is_valid
        self.loss = loss
        self.batch_size = batch_size
        self.epochs = epochs

        self.training_data = [feature[is_train], target_panda.values[is_train]]
        self.subvalid_data = [feature[is_subvalid], target_panda.values[is_subvalid]]
        self.valid_data = [feature[is_valid], target_panda.values[is_valid]]

        logger.info("training_data shape: " + str(self.training_data[0].shape))
        logger.info("subvalid_data shape: " + str(self.subvalid_data[0].shape))
        logger.info("valid_data shape: " + str(self.valid_data[0].shape))


    def loss_func(self, true, predicted):
        from keras import backend as K
        import keras.backend.tensorflow_backend as ktf
        epsilon = 0.1
        summ = ktf.maximum(tf.abs(true) + ktf.abs(predicted) + epsilon, 0.5 + epsilon)
        smape = ktf.abs(predicted - true) / summ * 2.0
        return smape

    def build_basic_model(self, training_data, subvalid_data, valid_data):

        train_x, train_y = training_data[0], training_data[1]
        subvalid_x, subvalid_y = subvalid_data[0], subvalid_data[1]
        valid_x, valid_y = valid_data[0], valid_data[1]

        logger.info("training_data shape: " + str(train_x.shape))
        feature_dim = train_x.shape[1]
        outputs_dim = train_y.shape[1]

        inputs = Input(shape=(feature_dim,))
        
        dense_1 = Dense(4096)(inputs)
        dense_1_output = Activation('selu')(dense_1)

        dense_2 = Dense(2048)(dense_1_output)
        dense_2_output = Activation('selu')(dense_2)

        dense_3 = Dense(512)(dense_2_output)
        dense_3_output = Activation('selu')(dense_3)
        outputs = Dense(outputs_dim, activation='relu')(dense_3_output)

        
        optimizer = Adam(lr=0.0001)
        smape_callback = Smape_callback((train_x, train_y), (subvalid_x, subvalid_y), (valid_x, valid_y))
        model = Model(inputs = inputs, outputs = outputs)
        model.compile(loss = self.loss_func, optimizer = optimizer)
        model.summary()
        model.fit(train_x, 
                  train_y, 
                  validation_data=(subvalid_x, subvalid_y), 
                  batch_size=self.batch_size, 
                  epochs=self.epochs, 
                  callbacks=[smape_callback])

        return model, smape_callback.best_sub_score, smape_callback.best_score, smape_callback.best_epoch

    def fine_tune(self, model, training_data, subvalid_data, valid_data):
        train_x, train_y = training_data[0], training_data[1]
        subvalid_x, subvalid_y = subvalid_data[0], subvalid_data[1]
        valid_x, valid_y = valid_data[0], valid_data[1]

        optimizer = Adam(lr=0.0001)
        model.compile(loss = self.loss_func, optimizer = optimizer)
        smape_callback = Smape_callback((train_x, train_y), (subvalid_x, subvalid_y), (valid_x, valid_y))

        model.fit(valid_x,
                  valid_y,
                  validation_data=(subvalid_x, subvalid_y),
                  batch_size=self.batch_size,
                  epochs=1,
                  callbacks=[smape_callback])
        return smape_callback.best_model, smape_callback.best_sub_score, smape_callback.best_score, smape_callback.best_epoch