from keras.callbacks import Callback
from keras.models import clone_model
import numpy as np

import argparse

class Smape_callback(Callback):
    def __init__(self, training_data, sub_validation_data, validation_data):
        self.x = training_data[0]
        self.y = training_data[1]
        self.x_subval = sub_validation_data[0]
        self.y_subval = sub_validation_data[1]
        self.x_val = validation_data[0]
        self.y_val = validation_data[1]
        self.best_sub_score = 1000.0
        self.best_score = 1000.0
        self.best_epoch = 0
        self.best_model = []
        self.last_epoch = 0

    @staticmethod
    def SMAPE_point(ans, pred):
        if ans == 0:
            ans += 0.03
        return 100 * abs(float(ans) - float(pred)) / ((abs(ans) + abs(pred))/ 2.0)

    @staticmethod
    def SMAPE(ans, pred):
        ans = np.clip(ans, 0.01, None)
        return np.mean(200 * abs(ans - pred) / (abs(ans) + abs(pred)))

    def on_train_begin(self, logs={}):
        return

    def on_train_end(self, logs={}):
        return

    def on_epoch_begin(self, epoch, logs={}):
        return

    def on_epoch_end(self, epoch, logs={}):
        y_pred_subval = self.model.predict(self.x_subval)

        subvalid_smape = self.SMAPE(self.y_subval, y_pred_subval)
        if subvalid_smape < self.best_sub_score:
            self.best_sub_score = subvalid_smape

            y_pred_val = self.model.predict(self.x_val)
            self.best_score = self.SMAPE(self.y_val, y_pred_val)

            self.best_epoch = epoch
            if (epoch > 30 and self.best_score < 70) or self.best_model == []:
                self.best_model = clone_model(self.model)
                self.best_model.set_weights(self.model.get_weights())
        self.last_epoch = epoch
        print("--subvalid_smape: %f: --best_sub_score: %f --best_score: %f" %(subvalid_smape, self.best_sub_score, self.best_score))
        print("")
        return

    def on_batch_begin(self, batch, logs={}):
        return

    def on_batch_end(self, batch, logs={}):
        return