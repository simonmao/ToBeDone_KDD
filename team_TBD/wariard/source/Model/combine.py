#data
import pandas as pd
import numpy as np

import argparse

#args
def arg_parse():
    parser = argparse.ArgumentParser()
    parser.add_argument('--submission_dir', default='../../submission/subset/', type=str)
    parser.add_argument('--bj_PM25', default='dataset_1_1')
    parser.add_argument('--bj_PM10', default='dataset_2_1')
    parser.add_argument('--bj_O3', default='dataset_3_1')
    parser.add_argument('--ld_PM25', default='dataset_4_1')
    parser.add_argument('--ld_PM10', default='dataset_5_1')
    parser.add_argument('--output_path', default='../../submission/')
    parser.add_argument('--end_time', default='2018-05-01-23')
    parser.add_argument('--submit_type', default="test")
    parser.add_argument('--No', default="03")
    args = parser.parse_args()
    return args

def clip(data_panda, label):
    data_panda[label] = np.where(data_panda[label] == 0, np.nan, data_panda[label])
    data_panda[label] = data_panda[label].interpolate(method='linear')
    data_panda[label] = data_panda[label].fillna(method='bfill')
    return data_panda

if __name__ == '__main__':
    args = arg_parse()
    if args.submit_type == "test":
        name = ""
    else:
        name = "_"+args.submit_type

    bj_PM25 = pd.read_csv(args.submission_dir+args.end_time+"/"+args.bj_PM25+"/bj_PM25"+name+".csv")
    bj_PM25 = clip(bj_PM25, "PM25")
    bj_PM10 = pd.read_csv(args.submission_dir+args.end_time+"/" +args.bj_PM10+"/bj_PM10"+name+".csv")
    bj_PM10 = clip(bj_PM10, "PM10")
    bj_O3 = pd.read_csv(args.submission_dir+args.end_time+"/" +args.bj_O3+"/bj_O3"+name+".csv")
    bj_O3 = clip(bj_O3, "O3")

    ld_PM25 = pd.read_csv(args.submission_dir+args.end_time+"/" +args.ld_PM25+"/ld_PM25"+name+".csv")
    ld_PM25 = clip(ld_PM25, "PM25")
    ld_PM10 = pd.read_csv(args.submission_dir+args.end_time+"/" +args.ld_PM10+"/ld_PM10"+name+".csv")
    ld_PM10 = clip(ld_PM10, "PM10")
    
    bj_subans = pd.merge(bj_PM25, bj_PM10, on=['submit_date', 'test_id'])
    bj_ans = pd.merge(bj_subans, bj_O3, on=['submit_date', 'test_id'])

    ld_ans = pd.merge(ld_PM25, ld_PM10, on=['submit_date', 'test_id'])

    station_name_list = ['CD1', 'BL0', 'GR4', 'MY7', 'HV1', 'GN3', 'GR9', 'LW2', 'GN0', 'KF1', 'CD9', 'ST5', 'TH4' ] 
    station_list = []
    for station_name in station_name_list:
        station_list += [station_name+"#"+str(i) for i in range(48)]

    is_forecast = ld_ans['test_id'].isin(station_list)
    ld_ans = ld_ans[is_forecast]

    ans = pd.concat([bj_ans, ld_ans]).fillna(0)
    ans = ans.rename(index=str, columns={"PM25": "PM2.5"})

    if args.submit_type == "test":
        ans.to_csv(args.output_path+'test/'+args.end_time+"_"+args.No+".csv", index=False, columns=['submit_date', 'test_id', 'PM2.5', 'PM10', 'O3'])
    elif args.submit_type == "ensemble":
        ans.to_csv(args.output_path+'ensemble/'+args.end_time+"_"+args.No+".csv", index=False, columns=['submit_date', 'test_id', 'PM2.5', 'PM10', 'O3'])
