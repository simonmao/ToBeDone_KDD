cd /home/guest/wariard/KDD/main_challenge/source/cronjob_code/
rm log.txt
export PATH="$HOME/.pyenv/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"
pyenv activate venv3.6.2
today=$(date "+%Y-%m-%d-%H" -u)
today_date=$(date "+%Y-%m-%d" -u)
yesterday_date=$(date -d yesterday "+%Y-%m-%d" -u)
tomorrow_date=$(date -d tomorrow "+%Y-%m-%d" -u)

mkdir -p "/home/guest/wariard/KDD/main_challenge/data/external_data/rainbow/"$today

python3 crawl_raw_data.py --end_time $today --city bj --aq aq &
#python3 crawl_raw_data.py --end_time $today --city bj --aq meo &
python3 crawl_raw_data.py --end_time $today --city ld --aq aq &
#python3 crawl_raw_data.py --end_time $today --city ld --aq meo &
#python3 crawl_rainbow.py $today &
wait

python3 crawl_bj_external_data.py "/home/guest/wariard/KDD/main_challenge/data/crawl_data/bj_aq_"$today".csv" "/home/guest/wariard/KDD/main_challenge/data/external_data/central/china_sites_20180529.csv" $today "/home/guest/wariard/KDD/main_challenge/data/crawl_data/bj_aq_"$today".csv"
python3 crawl_bj_external_data.py "/home/guest/wariard/KDD/main_challenge/data/crawl_data/bj_aq_"$today".csv" "/home/guest/wariard/KDD/main_challenge/data/external_data/central/china_sites_20180530.csv" $today "/home/guest/wariard/KDD/main_challenge/data/crawl_data/bj_aq_"$today".csv"

#scp -r "/home/guest/wariard/KDD/main_challenge/data/external_data/rainbow/"$today "kdd2018_team5@140.112.31.185:/tmp2/TBD/external_data/rainbow/"

python3 preprocessing.py --interpolation_method linear --k 4 --end_time $today --city bj --aq aq &
#python3 preprocessing.py --interpolation_method linear --k 4 --end_time $today --city bj --aq meo &
python3 preprocessing.py --interpolation_method linear --k 4 --end_time $today --city ld --aq aq &
#python3 preprocessing.py --interpolation_method linear --k 4 --end_time $today --city ld --aq meo &
wait


rm /home/guest/wariard/KDD/data/raw_data/bj_worldweather.csv
rm /home/guest/wariard/KDD/data/raw_data/ld_worldweather.csv
wget "https://learner.csie.ntu.edu.tw/~kevinorjohn/"$tomorrow_date"/bj_hourly_worldweather.csv"
mv /home/guest/wariard/KDD/main_challenge/source/cronjob_code/bj_hourly_worldweather.csv /home/guest/wariard/KDD/main_challenge/data/raw_data/bj_worldweather.csv
wget "https://learner.csie.ntu.edu.tw/~kevinorjohn/"$tomorrow_date"/ld_hourly_worldweather.csv"
mv /home/guest/wariard/KDD/main_challenge/source/cronjob_code/ld_hourly_worldweather.csv /home/guest/wariard/KDD/main_challenge/data/raw_data/ld_worldweather.csv 

today_date_without_year=$(date "+%m-%d" -u)
mkdir -p "./"$today_date_without_year
cp "/home/guest/wariard/KDD/main_challenge/data/crawl_data/bj_aq_"$today".csv" "./"$today_date_without_year"/bj_aq_"$today_date_without_year".csv"
cp "/home/guest/wariard/KDD/main_challenge/data/crawl_data/ld_aq_"$today".csv" "./"$today_date_without_year"/ld_aq_"$today_date_without_year".csv"
cp "/home/guest/wariard/KDD/main_challenge/data/raw_data/bj_worldweather.csv " "./"$today_date_without_year"/bj_worldweather.csv"
cp "/home/guest/wariard/KDD/main_challenge/data/raw_data/ld_worldweather.csv " "./"$today_date_without_year"/ld_worldweather.csv"
scp -r "./"$today_date_without_year "kdd2018_team5@140.112.31.185:~/data/"
rm -r "./"$today_date_without_year

rm /home/guest/wariard/KDD/data/raw_data/bj_uvindex.csv
rm /home/guest/wariard/KDD/data/raw_data/ld_uvindex.csv
wget "https://www.csie.ntu.edu.tw/~b03902055/kdd/bj_uvindex.csv"
mv /home/guest/wariard/KDD/main_challenge/source/cronjob_code/bj_uvindex.csv /home/guest/wariard/KDD/main_challenge/data/raw_data/bj_uvindex.csv
wget "https://www.csie.ntu.edu.tw/~b03902055/kdd/ld_uvindex.csv"
mv /home/guest/wariard/KDD/main_challenge/source/cronjob_code/ld_uvindex.csv /home/guest/wariard/KDD/main_challenge/data/raw_data/ld_uvindex.csv

cd /home/guest/wariard/KDD/ToBeDone/
yesterday=$(cat ./time.txt)
sh change_source_config.sh $yesterday $today
cp ./.feagenrc/cronjob_config/* ./.feagenrc/april_dataset_config/$today/


for i in $(seq 1 5)
do
    sed -i 's/testyesterday/'$yesterday_date'/g' "./.feagenrc/april_dataset_config/"$today"/dataset_"$i"_1.yml"
    sed -i 's/testtomorrow/'$tomorrow_date'/g' "./.feagenrc/april_dataset_config/"$today"/dataset_"$i"_1.yml"
done

rm ./time.txt
echo $today > ./time.txt

for i in $(seq 1 5)
do
    sh gen_feagen.sh $i 1 1
done

(cd /home/guest/wariard/KDD/main_challenge/source/Feature_selection/;
sh run_DNN.sh 1 1 1 PM25 $today bj DNN 11;) &

(cd /home/guest/wariard/KDD/main_challenge/source/Feature_selection/;
sh run_DNN.sh 2 1 1 PM10 $today bj DNN 12;) &

(cd /home/guest/wariard/KDD/main_challenge/source/Feature_selection/;
sh run_DNN.sh 3 1 1 O3 $today bj DNN 11;) &

(cd /home/guest/wariard/KDD/main_challenge/source/Feature_selection/;
sh run_DNN.sh 4 1 1 PM25 $today ld DNN 11;) & 

(cd /home/guest/wariard/KDD/main_challenge/source/Feature_selection/;
sh run_DNN.sh 5 1 1 PM10 $today ld DNN 9;) &
wait

#(cd /home/guest/wariard/KDD/main_challenge/source/Feature_selection/;
#sh run_DNN.sh 6 1 1 PM25 $today bj DNN 14;) &#

#(cd /home/guest/wariard/KDD/main_challenge/source/Feature_selection/;
#sh run_DNN.sh 7 1 1 PM10 $today bj DNN 7;) &#

#(cd /home/guest/wariard/KDD/main_challenge/source/Feature_selection/;
#sh run_DNN.sh 8 1 1 O3 $today bj DNN 13;) &#

#(cd /home/guest/wariard/KDD/main_challenge/source/Feature_selection/;
#sh run_DNN.sh 9 1 1 PM25 $today ld DNN 15;) & #

#(cd /home/guest/wariard/KDD/main_challenge/source/Feature_selection/;
#sh run_DNN.sh 10 1 1 PM10 $today ld DNN 10;) &
#wait

cd /home/guest/wariard/KDD/main_challenge/source/Feature_selection/
python3 combine.py --end_time $today --submit_type test
python3 combine.py --end_time $today --submit_type ensemble
python3 combine.py --end_time $today --submit_type internal

cd /home/guest/wariard/KDD/main_challenge/source/cronjob_code/
#python3 clip.py "/home/guest/wariard/KDD/main_challenge/submission/test/"$today"_03.csv" "/home/guest/wariard/KDD/main_challenge/submission/test/"$today"_03.csv"

#python3 combine.py --end_time $today --bj_PM25 dataset_6_1 --bj_PM10 dataset_7_1 --bj_O3 dataset_8_1 --No 05 --submit_type test
#python3 combine.py --end_time $today --bj_PM25 dataset_6_1 --bj_PM10 dataset_7_1 --bj_O3 dataset_8_1 --No 05 --submit_type ensemble
#python3 combine.py --end_time $today --bj_PM25 dataset_6_1 --bj_PM10 dataset_7_1 --bj_O3 dataset_8_1 --No 05 --submit_type internal

today_date_without_year=$(date "+%m-%d" -u)
scp "/home/guest/wariard/KDD/main_challenge/submission/test/"$today"_03.csv" "kdd2018_team5@140.112.31.185:/tmp2/TBD/test/test_"$today_date_without_year"_03.csv"
scp "/home/guest/wariard/KDD/main_challenge/submission/ensemble/"$today"_03.csv" "kdd2018_team5@140.112.31.185:/tmp2/TBD/valid/valid_"$today_date_without_year"_03.csv"
#scp "/home/guest/wariard/KDD/main_challenge/submission/test/"$today"_05.csv" "kdd2018_team5@140.112.31.185:/tmp2/TBD/test/test_"$today_date_without_year"_05.csv"
#scp "/home/guest/wariard/KDD/main_challenge/submission/ensemble/"$today"_05.csv" "kdd2018_team5@140.112.31.185:/tmp2/TBD/valid/valid_"$today_date_without_year"_05.csv"

pyenv deactivate

