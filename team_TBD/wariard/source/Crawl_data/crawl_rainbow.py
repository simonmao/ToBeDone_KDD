import json
import requests
import sys
from info import *

import logging
logging.basicConfig(format='%(asctime)s [%(levelname)s] [%(filename)s:%(lineno)s - %(funcName)s() ] %(message)s')
logger = logging.getLogger()
logger.setLevel(logging.INFO)

token = "ZiARx2Gd7cyvEjAV"

def query(dictionary):
    for station_id in dictionary:
        logger.info("crawl_rainbow station: " + str(station_id))
        if "longitude" in dictionary[station_id]:
            url = "https://api.caiyunapp.com/v2/"+token+"/"+str(dictionary[station_id]["longitude"])+","+str(dictionary[station_id]["latitude"])+"/forecast.json"
            response = False
            while not response:
                try:
                    response = requests.get(url, timeout=5)
                except:
                    response = False
            if response and response.ok:
                with open("/home/guest/wariard/KDD/main_challenge/data/external_data/rainbow/"+sys.argv[1]+"/"+sys.argv[1]+"_"+station_id, "w") as f:
                    json.dump(response.text, f)
            else:
              logger.info("station "+str(station_id) + " failed!")


query(bj_aq_station_dic)
query(ld_aq_station_dic)