xgboost
lightgbm
sklearn
keras==2.1.5
tensorflow-gpu==1.0.0

pandas
joblib
h5py
pyyaml
tables
