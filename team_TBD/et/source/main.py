import xgboost as xgb
import lightgbm as lgb
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import ExtraTreesRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.ensemble import AdaBoostRegressor
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import SGDRegressor
from sklearn.svm import SVR
from sklearn.svm import NuSVR
from sklearn.svm import LinearSVR

from sklearn.model_selection import train_test_split
from sklearn.model_selection import ParameterGrid
from sklearn.metrics import mean_absolute_error
from sklearn.externals import joblib
from sklearn.preprocessing import MinMaxScaler

import pandas as pd
import time
import os
import sys
import logging
import argparse
import random
import json
import h5py
import yaml

from model_config import ModelConfig

from utils import get_logger, print_config, read_config
from utils import symmetric_mean_absolute_percentage_error
from utils import build_submission_dataframe, write_submission


random.seed(5314) # for reproducibility
import numpy as np
np.random.seed(5314) # for reproducibility


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--dataset_name', required=True, type=str)
    parser.add_argument('--submit_type', required=True, type=str)
    parser.add_argument('--config_id', type=int)
    ## task argument
    parser.add_argument('--ld_PM25', type=str)
    parser.add_argument('--ld_PM10', type=str)
    parser.add_argument('--bj_PM25', type=str)
    parser.add_argument('--bj_PM10', type=str)
    parser.add_argument('--bj_O3', type=str)
    parser.add_argument('--ld_PM25_station_split', action='store_true')
    parser.add_argument('--ld_PM10_station_split', action='store_true')
    parser.add_argument('--bj_PM25_station_split', action='store_true')
    parser.add_argument('--bj_PM10_station_split', action='store_true')
    parser.add_argument('--bj_O3_station_split', action='store_true')
    parser.add_argument('--ld_PM25_hour_split', action='store_true')
    parser.add_argument('--ld_PM10_hour_split', action='store_true')
    parser.add_argument('--bj_PM25_hour_split', action='store_true')
    parser.add_argument('--bj_PM10_hour_split', action='store_true')
    parser.add_argument('--bj_O3_hour_split', action='store_true')

    parser.add_argument('--bundles_dir', type=str, required=True)
    parser.add_argument('--regressor', type=str, default='et')
    parser.add_argument('--submit_dir', type=str, default="../submit")
    parser.add_argument('--submit_path', type=str, default="")
    parser.add_argument('--config_dir', type=str, default="../config")
    
    parser.add_argument('--submit', action='store_true')
    args = parser.parse_args()
    print(json.dumps(vars(args), indent=2, sort_keys=True))
    return args


def build_regressor():
    
    # if args.regressor == "lr":
        # regressor = LinearRegression(n_jobs=-1)
    if args.regressor == "dt":
        regressor = DecisionTreeRegressor(criterion='mse', random_state=0)
    if args.regressor == "et":
        regressor = ExtraTreesRegressor(criterion='mse', n_jobs=-1, random_state=0)
    if args.regressor == "rf":
        regressor = RandomForestRegressor(criterion='mse', n_jobs=-1, random_state=0)
    if args.regressor == "gb":
        regressor = GradientBoostingRegressor(criterion='mse', random_state=0)
    if args.regressor == "ada":
        regressor = AdaBoostRegressor( random_state=0)
    if args.regressor == "svr":
        regressor = SVR(kernel='rbf')
    if args.regressor == "lr_svr":
        regressor = LinearSVR(random_state=0)
    if args.regressor == 'xgb':
        regressor = xgb.XGBRegressor(n_jobs=-1, random_state=0)
    if args.regressor == 'lgb':
        regressor = lgb.LGBMRegressor(objective='regression', n_jobs=-1, random_state=0)

    return regressor


def grid_search(X_subtrain, y_subtrain, X_valid, y_valid,
                X_train, y_train, X_test, y_test):
    
    if args.regressor == "dt":
        grid = { 'max_depth': [ 2, 4, 6]}
    if args.regressor == "et":
        grid = {'n_estimators': [ 3, 5], 'max_depth': [ 8, 10]}
    if args.regressor == "rf":
        grid = {'n_estimators': [ 300, 500], 'max_depth': [ 8, 10]}
    if args.regressor == "gb":
        grid = {'n_estimators': [100, 200], 'max_depth': [3, 5]}
    if args.regressor == "ada":
        grid = {'n_estimators': [50, 100] }
    if args.regressor == "svr":
        grid = {"C": [1e0, 1e1, 1e2, 1e3], "gamma": np.logspace(-2, 2, 5)}
    if args.regressor == "lr_svr":
        grid = {"C": [1e0, 1e1, 1e2, 1e3], "gamma": np.logspace(-2, 2, 5)}
    if args.regressor == 'xgb':
        grid =  {'max_depth': [2,4,6], 'n_estimators': [50,100,200]}
        # grid =  {'max_depth': [2], 'n_estimators': [100]}
    if args.regressor == 'lgb':
        grid =  { 'n_estimators': [50,100]}

    logger.info(str(grid))
    best_SMAPE = 1000
    for g in ParameterGrid(grid):
        regressor = build_regressor()
        regressor.set_params(**g)
        # regressor.fit(X_train, y_train, sample_weight=1/y_train)
        regressor.fit(X_subtrain, y_subtrain)
        y_pred = regressor.predict(X_valid)
        # y_valid_weight = np.clip(y_valid, a_min=0.001, a_max=None)
        # y_pred = np.clip(y_valid, a_min=0.001, a_max=None)
        # MAPE = mean_absolute_error(y_valid, y_pred, sample_weight=1/y_valid_weight)
        SMAPE = symmetric_mean_absolute_percentage_error(y_valid, y_pred)
        if SMAPE < best_SMAPE:
            best_SMAPE = SMAPE
            best_grid = g
            best_y_pred = y_pred
            best_regressor = regressor
        logger.info("\t\t{}".format(str(g)))
        # print(regressor)
        logger.info("\t\tSMAPE : {}".format(SMAPE))
    logger.info("\tbest grid : {}".format(str(best_grid)))
    logger.info("\tbest SMAPE : {}".format(best_SMAPE))
    
    regressor = build_regressor()
    regressor.set_params(**best_grid)
    # regressor.fit(X_train, y_train, sample_weight=1/y_train)
    regressor.fit(X_train, y_train)
    y_test_pred = regressor.predict(X_test)
    
    return best_grid, best_y_pred, y_test_pred, regressor.feature_importances_

def station_split(subtrain_features, subtrain_df,
                    valid_features, valid_df,
                    train_features, train_df,
                    test_features, test_df, is_hour_split):
    
    station_ids = valid_df.index.get_level_values('station_id').unique()
    
    valid_frames, test_frames = [], []
    for station_id in station_ids:
        is_subtrain_station = subtrain_df.index.get_level_values('station_id') == station_id
        station_subtrain_features = subtrain_features[is_subtrain_station]
        station_subtrain_df = subtrain_df[is_subtrain_station]
        
        is_valid_station = valid_df.index.get_level_values('station_id') == station_id
        station_valid_features = valid_features[is_valid_station]
        station_valid_df = valid_df[is_valid_station]

        is_train_station = train_df.index.get_level_values('station_id') == station_id
        station_train_features = train_features[is_train_station]
        station_train_df = train_df[is_train_station]

        is_test_station = test_df.index.get_level_values('station_id') == station_id
        station_test_features = test_features[is_test_station]
        station_test_df = test_df[is_test_station]
      
        if is_hour_split:

            (station_valid_pred_df,
            station_test_pred_df, 
            feature_importances_df) = hour_split(station_subtrain_features, station_subtrain_df,
                                station_valid_features, station_valid_df,
                                station_train_features, station_train_df,
                                station_test_features, station_test_df)

        else:
            
            (station_valid_pred_df, 
             station_test_pred_df,
            feature_importances_df) = finetune_predict(station_subtrain_features, station_subtrain_df,
                                station_valid_features, station_valid_df,
                                    station_train_features, station_train_df,
                                    station_test_features, station_test_df)
        
        valid_frames.append(station_valid_pred_df)
        test_frames.append(station_test_pred_df)
    
    valid_pred_df = pd.concat(valid_frames)
    test_pred_df = pd.concat(test_frames)
    
    return valid_pred_df, test_pred_df, feature_importances_df

def hour_split(subtrain_features, subtrain_df,
                    valid_features, valid_df,
                    train_features, train_df,
                    test_features, test_df):
               
    valid_frames, test_frames = [], []

    interval = 1
    hour_inedx = np.arange(48).reshape(-1, interval)

    for hours in hour_inedx:
               
        hour_subtrain_df = subtrain_df[hours]
        hour_valid_df = valid_df[hours]
        hour_train_df = train_df[hours]
        hour_test_df = test_df[hours]
    
    # for hour in range(48):
               
        # hour_subtrain_df = subtrain_df[[hour]]
        # hour_valid_df = valid_df[[hour]]
        # hour_train_df = train_df[[hour]]
        # hour_test_df = test_df[[hour]]
        
        # if is_station_split:

            # hour_pred_df = station_split(subtrain_features, hour_subtrain_df,
                                        # valid_features, hour_valid_df)

        # else:
        (hour_valid_pred_df,
        hour_test_pred_df, 
        feature_importances_df) = finetune_predict(subtrain_features, hour_subtrain_df,
                                        valid_features, hour_valid_df,
                                        train_features, hour_train_df,
                                        test_features, hour_test_df)
        
        valid_frames.append(hour_valid_pred_df)
        test_frames.append(hour_test_pred_df)
    
    valid_pred_df = pd.concat(valid_frames, axis=1)
    test_pred_df = pd.concat(test_frames, axis=1)
    
    return valid_pred_df, test_pred_df, feature_importances_df


def finetune_predict(subtrain_features, subtrain_df,
                        valid_features, valid_df,
                        train_features, train_df,
                        test_features, test_df):
    
    subtrain_labels = subtrain_df.values
    valid_labels = valid_df.values
    train_labels = train_df.values
    test_labels = test_df.values


    best_param, valid_preds, test_preds, feature_importances = grid_search(subtrain_features, subtrain_labels, 
                       valid_features, valid_labels,
                       train_features, train_labels,
                       test_features, test_labels)

    valid_pred_df = pd.DataFrame(data = valid_preds, 
                       columns= valid_df.columns,
                       index= valid_df.index)

    test_pred_df = pd.DataFrame(data = test_preds, 
                       columns= test_df.columns,
                       index= test_df.index)

    feature_importances_df = pd.DataFrame(data = feature_importances.reshape(1,-1))
    
    return valid_pred_df, test_pred_df, feature_importances_df

def build_SMAPE_dataframe(df, pred_df):
    
    station_ids = df.index.get_level_values('station_id').unique()
    dates = df.index.get_level_values('date').unique().normalize()
    SMAPE_df = pd.DataFrame(index=station_ids, columns=dates)

    for station_id in station_ids:
        for date in dates:
            station_id_filter = df.index.get_level_values('station_id') == station_id
            date_filter = df.index.get_level_values('date') == date
            y_test = df[station_id_filter & date_filter].values
            y_pred = pred_df[station_id_filter & date_filter].values
            SMAPE_df.loc[station_id, date] = symmetric_mean_absolute_percentage_error(y_test, y_pred)

    for station_id in station_ids:
        station_id_filter = df.index.get_level_values('station_id') == station_id
        y_test = df[station_id_filter].values.reshape(-1)
        y_pred = pred_df[station_id_filter].values.reshape(-1)
        SMAPE_df.loc[station_id, 'all_dates'] = symmetric_mean_absolute_percentage_error(y_test, y_pred)
        
    y_test = df.values.reshape(-1)
    y_pred = pred_df.values.reshape(-1)
    SMAPE_df['all_stations'] = symmetric_mean_absolute_percentage_error(y_test, y_pred) 

    return SMAPE_df

regressor_dict = {
        "dt": "DecisionTree",
        "et": "ExtraTrees",
        "rf": "RandomForest",
        "gb": "GradientBoost",
        "ada": "AdaBoost",
        "xgb": "XGB",
        "lgb": "LGB"
                 }

## utils
args = parse_args()
logger = get_logger()


#### regressor name
## ex. ExtraTrees
regressor_name = regressor_dict[args.regressor]

#### read model config
## ex. config_dataset_internal1_0001.yml
timestamp = time.strftime("%Y%m%dT%H%M%S", time.localtime())
dataset_name = args.dataset_name
submit_type = args.submit_type
config_name = "config_{}_{}_{}".format(dataset_name, submit_type, timestamp)
config_path = os.path.join(args.config_dir, config_name+'.yml')
logger.info("Read config")
logger.info("Config name : {}".format(config_name))
model_cfg = ModelConfig(config_path, args=args)
config = model_cfg.cfg

#### read target_name from data_bundles
logger.info("Read Target")
## for city : bj, ld
cities = list(config['task'].keys())
for city in cities:
    ## for pollutants : PM25, PM10, O3
    pollutants = list(config['task'][city].keys())
    for pollutant in pollutants:
        bundles_file = config['task'][city][pollutant]['bundles_file']
        bundles_path = os.path.join(args.bundles_dir, bundles_file)
        print(args.bundles_dir)
        print(bundles_file)
        logger.info("Bundles path : ".format(bundles_path))
        data_bundles = h5py.File(bundles_path)
                
        for target_name in data_bundles['target'].keys():
            if pollutant in target_name and city in target_name:
                config['task'][city][pollutant]['target_name'] = target_name
print_config(config)

#### start model
logger.info("Start Model")
## for city : bj, ld
cities = list(config['task'].keys())
city_SMAPE_frames = []
city_valid_frames = []
city_test_frames = []
city_test_ans_frames = []
for city in cities:
    
    ## for pollutants : PM25, PM10, O3
    SMAPE_frames = []
    pollutant_valid_frames = []
    pollutant_test_frames = []
    pollutant_test_ans_frames = []
    pollutants = list(config['task'][city].keys())
    for pollutant in pollutants:
        
        ## for city and pollutant 
        logger.info("For city : {}  pollutant : {}".format(city, pollutant))
        print_config(config['task'][city][pollutant])
        bundles_file = config['task'][city][pollutant]['bundles_file']
        bundles_path = os.path.join(args.bundles_dir, bundles_file)
        data_bundles = h5py.File(bundles_path)

        #### read feature

        ## read concat feature
        # features = data_bundles['{}_features'.format(city)].value

        ## read split feature and concat by myself
        feature_list = []
        for key in data_bundles['features']:
            feature = data_bundles['features'][key].value
            if feature.ndim == 2:
                feature_list.append(feature)
            else:
                feature_list.append(feature.reshape(-1,1))
        feature_names = [ key for key in data_bundles['features'] ]
        features = np.concatenate(feature_list, axis=1)

        #### normalize
        # scale feature to [0, 1]
        scaler = MinMaxScaler()
        features = scaler.fit_transform(features)

        #### read target from data_bundles
        target_path = '/target/{}'.format(config['task'][city][pollutant]['target_name'])
        target_df = pd.read_hdf(bundles_path, target_path)

        is_subtrain = data_bundles['subtraining_filters'].value
        subtrain_features = features[is_subtrain]
        subtrain_df = target_df[is_subtrain]

        is_valid = data_bundles['validation_filters'].value
        valid_features = features[is_valid]
        valid_df = target_df[is_valid]
        
        is_train = data_bundles['training_filters'].value
        train_features = features[is_train]
        train_df = target_df[is_train]

        is_test = data_bundles['testing_filters'].value
        test_features = features[is_test]
        test_df = target_df[is_test]

        print("subtrain_features shape" + str(subtrain_features.shape))
        print("subtrain_labels shape" + str(subtrain_df.shape))
        print("valid_features shape" + str(valid_features.shape))
        print("valid_labels shape" + str(valid_df.shape))
        print("train_features shape" + str(train_features.shape))
        print("train_labels shape" + str(train_df.shape))
        print("test_features shape" + str(test_features.shape))
        print("test_labels shape" + str(test_df.shape))

        is_station_split = config['task'][city][pollutant]['station_split']
        is_hour_split = config['task'][city][pollutant]['hour_split']

        if is_station_split:
                
            (valid_pred_df, test_pred_df,
            feature_importances_df) = station_split(subtrain_features, subtrain_df,
                                    valid_features, valid_df,
                                    train_features, train_df,
                                    test_features, test_df, is_hour_split)

        else:
            
            if is_hour_split:

                (valid_pred_df, test_pred_df,
                feature_importances_df) = hour_split(subtrain_features, subtrain_df,
                                    valid_features, valid_df,
                                    train_features, train_df,
                                    test_features, test_df)

            else:
                
                valid_pred_df, test_pred_df, feature_importances_df = finetune_predict(
                                    subtrain_features, subtrain_df,
                                    valid_features, valid_df,
                                    train_features, train_df,
                                    test_features, test_df)



        ###### SMAPE df
        SMAPE_df = build_SMAPE_dataframe(test_df, test_pred_df)
        SMAPE_frames.append(SMAPE_df)
        
        ###### target df
        pollutant_valid_frames.append(valid_pred_df)
        pollutant_test_frames.append(test_pred_df)

    ##### SMAPE df
    city_SMAPE_df = pd.concat(SMAPE_frames, keys=pollutants, names=['pollutant'])
    city_SMAPE_frames.append(city_SMAPE_df)

    ###### target_df
    city_valid_df = pd.concat(pollutant_valid_frames, keys=pollutants, names=['pollutant'])
    city_valid_frames.append(city_valid_df)
    city_test_df = pd.concat(pollutant_test_frames, keys=pollutants, names=['pollutant'])
    city_test_frames.append(city_test_df)

##### SMAPE df
all_SMAPE_df = pd.concat(city_SMAPE_frames, keys=cities, names=['city'])
all_SMAPE_df = pd.concat([all_SMAPE_df],  keys=[config_name], names=['config'])

##### target_df
all_valid_df = pd.concat(city_valid_frames, keys=cities, names=['city'])
all_test_df = pd.concat(city_test_frames, keys=cities, names=['city'])


##### build submission dataframe
output_test_pred_df = build_submission_dataframe(all_test_df)

##############
# submission #
##############

if args.submit:
    # ex. '../submit/internal1/submission-config_dataset_internal1_0001-ExtraTrees.csv'
    submit_file = 'submission-{}-{}.csv'.format(config_name, regressor_name)
    submit_path = os.path.join(args.submit_dir, config['submit_type'], submit_file)
    if args.submit_path:
        submit_path = args.submit_path
    write_submission(output_test_pred_df, submit_path)
    
