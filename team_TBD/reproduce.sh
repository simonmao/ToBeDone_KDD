#Usage: ./reproduce.sh [date] Ex: ./reproduce.sh 05-01

TBD_dir=`pwd`
date=$1

## ExtraTrees
## model_num : 31
cd $TBD_dir/et/source
bash all.sh $date

## NN
cd $TBD_dir/wariard/source
bash reproduce.sh 2018-$date $date
