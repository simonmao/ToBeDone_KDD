import pandas as pd
import numpy as np
from datetime import datetime, timedelta, date

def remove_unnamed(data):
    key = data.keys()
    for i in key:
        if i[0:7] == 'Unnamed' :
            data = data.drop([i],axis=1)
    return data

def get_internal_sample_sub_df(val_end_date):
    val_end_date = datetime.strptime(val_end_date, '%Y-%m-%d')
    df = pd.read_csv('/nfs/Sif/runaway/20180301_20180331_2cities_sample_submission.csv')
    df = df[df['submit_date']=='2018-02-28']
    val_start_date = datetime(2018, 3, 31)
    delta = val_end_date - val_start_date
    val_date = []
    for i in range(-(delta.days+3),-2):
        val_date.append((datetime.now() + timedelta(hours=-8,days=i)).strftime('%Y-%m-%d'))
    ret_df = df.iloc[0:0]
    for d in val_date:
        df['submit_date'] = df['submit_date'].apply(lambda x:d)
        df['test_id'] = df['test_id'].apply(lambda x: x.split('_aq')[0][:10] + '_aq' + x.split('_aq')[1] if 'aq' in x else x)
        ret_df = ret_df.append(df,ignore_index=True)
    #ret_df['name'] = ret_df['test_id'].apply(lambda x:x.split('#')[0])
    #ret_df = ret_df.sort_values(by=['name','submit_date'],axis=0)
    ret_df = ret_df[['submit_date','test_id','PM2.5','PM10','O3']]
    return ret_df

def pad_five_trash_days(data):
    last_time = data.iloc[-1]['time'].values[0]
    year  = int(last_time.split('-')[0])
    month = int(last_time.split('-')[1])
    date  = int(last_time.split('-')[2].split(' ')[0])
    hour  = int(last_time.split(':')[0].split(' ')[1])
    for i in range(240): #預先padding 10天的資料，以免抓不到
        hour += 1
        if hour == 24:
            date += 1
            hour = 0
        if (date == 32 and month in [1,3,5,7,8,10,12]) or (date == 31 and month in [4,6,9,11]) or (date == 29 and month == 2):
            date = 1
            month += 1
        if month == 13:
            year += 1
            month = 1
        name = '%d-%02d-%02d %02d:00:00'%(year,month,date,hour)
        empty = pd.Series(index=data.columns)
        empty['time'] = name
        data = data.append(empty,ignore_index=True)
    return data.fillna(method='ffill')

def get_ld_daybyday_data(start,end):
    training_days = 1
    pred_features = ['PM2.5','PM10']
    ld = pd.read_csv('/nfs/Sif/runaway/kdd_data/Mao/up_to_date_data/ld_train_007.csv')
    
    ld = remove_unnamed(ld)
    ld = ld.sort_values(by=['time','station'],axis=0)
    ld_x = ld.pivot('time', 'station').reset_index() 
    ld_y = ld[['time','station'] + pred_features].pivot('time', 'station').reset_index()
    ld_x = pad_five_trash_days(ld_x)
    ld_y = pad_five_trash_days(ld_y)
    
    (X_train, y_train) = ([],[])
    
    for i in range(int((len(ld_x))/24)):
        X = ld_x.iloc[i*24:i*24+training_days*24-2] 
        y = ld_y.iloc[i*24+training_days*24-2:i*24+training_days*24+48]
        time = X.iloc[-1]['time'].values[0].split(' ')[0]
        if time >= start and time <= end:
            #print(X)
            #print(y)
            X_train.append(X.drop(['time'],axis=1).values)
            y_train.append(y.drop(['time'],axis=1).values)
    print('Return London\'s %s to %s \'s day by day (X,y) !'%(start,end))
    return (np.array(X_train), np.array(y_train))

def ld_pred_to_internal_csv(y_pred,output_csv_path,end):
    stations = ['BL0', 'CD1', 'CD9', 'GN0', 'GN3', 'GR4', 'GR9', 'HV1', 'KF1', 'LW2', 'MY7', 'ST5', 'TH4']
    pred_features = ['PM2.5','PM10']
    y_key = ['%s_%s'%(st,pol)for pol in ['PM2.5','PM10'] for st in stations ]
    sample_sub = get_internal_sample_sub_df(end)
    day_range = sample_sub['submit_date'].value_counts().keys().sort_values()
    submission = sample_sub.iloc[0:0]
    if len(day_range) != len(y_pred):
        print('The length of sample submission(=%d) is not equal to the y_pred(=%d).'%(len(day_range),len(y_pred)))
        print('Failed to write to csv.')
        return None
    if y_pred.shape[1]!=48:
        print('You input %d hour instead of 48!'%(y_pred.shape[1]))
        return None
    for i,vec in enumerate(y_pred):
        date = day_range[i]
        tmp = pd.DataFrame(data=vec,columns=y_key)
        for hour,vec1 in tmp.iterrows():
            for st in stations:
                empty = pd.Series([date,'%s#%d'%(st,hour),vec1['%s_PM2.5'%(st)],vec1['%s_PM10'%(st)],0],index=['submit_date','test_id','PM2.5','PM10','O3'])
                submission = submission.append(empty,ignore_index=True)
    sample_sub = sample_sub.drop(['PM2.5','PM10','O3'],axis=1)
    pd.merge(submission,sample_sub,on=['submit_date','test_id'],how='inner').to_csv(output_csv_path,index=False)
    print('Finish write London internal csv to %s.'%(output_csv_path))
    return pd.merge(submission,sample_sub,on=['submit_date','test_id'],how='inner')

def ld_pred_to_submitable_csv(y_pred,output_csv_path):
    stations = ['BL0', 'CD1', 'CD9', 'GN0', 'GN3', 'GR4', 'GR9', 'HV1', 'KF1', 'LW2', 'MY7', 'ST5', 'TH4']
    y_key = ['%s_%s'%(st,pol)for pol in ['PM2.5','PM10'] for st in stations ]
    
    if len(y_pred)!=48:
        print('You input %d hour instead of 48!'%(y_pred.shape[1]))
        return None
    submission = pd.DataFrame([],columns = ['test_id','PM2.5','PM10','O3'])
    for hour,vec in enumerate(y_pred):
        vec = pd.Series(data=vec,index=y_key)
        for st in stations:
            empty = pd.Series(['%s#%d'%(st,hour),vec['%s_PM2.5'%(st)],vec['%s_PM10'%(st)],0],index=['test_id','PM2.5','PM10','O3'])
            submission = submission.append(empty,ignore_index=True)
    submission.to_csv(output_csv_path,index=False)
    print('Finish write London submittable csv to %s.'%(output_csv_path))
    return submission
