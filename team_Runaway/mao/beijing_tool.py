import pandas as pd
import numpy as np
from datetime import datetime, timedelta, date

def remove_unnamed(data):
    key = data.keys()
    for i in key:
        if i[0:7] == 'Unnamed' :
            data = data.drop([i],axis=1)
    return data

def get_internal_sample_sub_df(mao_path,val_end_date):
    val_end_date = datetime.strptime(val_end_date, '%Y-%m-%d')
    df = pd.read_csv(mao_path+'sam_sub.csv')
    df = df[df['submit_date']=='2018-02-28']
    val_start_date = datetime(2018, 3, 31)
    delta = val_end_date - val_start_date
    val_date = []
    for i in range(-(delta.days+3),-2):
        val_date.append((datetime.now() + timedelta(hours=-8,days=i)).strftime('%Y-%m-%d'))
    ret_df = df.iloc[0:0]
    for d in val_date:
        df['submit_date'] = df['submit_date'].apply(lambda x:d)
        df['test_id'] = df['test_id'].apply(lambda x: x.split('_aq')[0][:10] + '_aq' + x.split('_aq')[1] if 'aq' in x else x)
        ret_df = ret_df.append(df,ignore_index=True)
    #ret_df['name'] = ret_df['test_id'].apply(lambda x:x.split('#')[0])
    #ret_df = ret_df.sort_values(by=['name','submit_date'],axis=0)
    ret_df = ret_df[['submit_date','test_id','PM2.5','PM10','O3']]
    return ret_df

def pad_five_trash_days(data):
    last_time = data.iloc[-1]['time'].values[0]
    year  = int(last_time.split('-')[0])
    month = int(last_time.split('-')[1])
    date  = int(last_time.split('-')[2].split(' ')[0])
    hour  = int(last_time.split(':')[0].split(' ')[1])
    for i in range(240): #預先padding 10天的資料，以免抓不到
        hour += 1
        if hour == 24:
            date += 1
            hour = 0
        if (date == 32 and month in [1,3,5,7,8,10,12]) or (date == 31 and month in [4,6,9,11]) or (date == 29 and month == 2):
            date = 1
            month += 1
        if month == 13:
            year += 1
            month = 1
        name = '%d-%02d-%02d %02d:00:00'%(year,month,date,hour)
        empty = pd.Series(index=data.columns)
        empty['time'] = name
        data = data.append(empty,ignore_index=True)
    return data.fillna(method='ffill')

def get_bj_daybyday_data(start,end):
    bj = pd.read_csv('/nfs/Sif/runaway/kdd_data/Mao/up_to_date_data/bj_train_007.csv')
    training_days = 1
    pred_features = ['PM2.5','PM10','O3']
    bj = bj[bj['time'] >= '2017-01-02 00:00:00']
    bj = remove_unnamed(bj)
    bj = bj.sort_values(by=['time','station'],axis=0)
    bj_x = bj.pivot('time', 'station').reset_index() 
    bj_y = bj[['time','station'] + pred_features].pivot('time', 'station').reset_index()
    bj_x = pad_five_trash_days(bj_x)
    bj_y = pad_five_trash_days(bj_y)
    
    (X_train, y_train) = ([],[])
    
    for i in range(int((len(bj_x))/24)-3):
        X = bj_x.iloc[i*24:i*24+training_days*24-1] 
        y = bj_y.iloc[i*24+training_days*24-1:i*24+training_days*24+48]
        time = X.iloc[-1]['time'].values[0].split(' ')[0]
        if time >= start and time <= end:
            #print(X)
            #print(y)
            X_train.append(X.drop(['time'],axis=1).values)
            y_train.append(y.drop(['time'],axis=1).values)
    print('Return Beijing\'s %s to %s \'s day by day (X,y) !'%(start,end))
    return (np.array(X_train), np.array(y_train))

def bj_pred_to_internal_csv(path,y_pred,output_csv_path,end):
    stations = ['aotizhongx_aq', 'badaling_aq', 'beibuxinqu_aq', 'daxing_aq', 'dingling_aq', 
                'donggaocun_aq', 'dongsi_aq', 'dongsihuan_aq', 'fangshan_aq', 'fengtaihua_aq', 
                'guanyuan_aq', 'gucheng_aq', 'huairou_aq', 'liulihe_aq', 'mentougou_aq', 'miyun_aq', 
                'miyunshuik_aq', 'nansanhuan_aq', 'nongzhangu_aq', 'pingchang_aq', 'pinggu_aq', 
                'qianmen_aq', 'shunyi_aq', 'tiantan_aq', 'tongzhou_aq', 'wanliu_aq', 
                'wanshouxig_aq', 'xizhimenbe_aq', 'yanqin_aq', 'yizhuang_aq', 'yongdingme_aq', 
                'yongledian_aq', 'yufa_aq', 'yungang_aq', 'zhiwuyuan_aq']
    y_key = ['%s_%s'%(st,pol)for pol in ['PM2.5','PM10','O3'] for st in stations ]
    pred_features = ['PM2.5','PM10','O3']
    sample_sub = get_internal_sample_sub_df(path,end)
    
    day_range = sample_sub['submit_date'].value_counts().keys().sort_values()
    submission = sample_sub.iloc[0:0]
    if len(day_range) != len(y_pred):
        print('The length of sample submission(=%d) is not equal to the y_pred(=%d).'%(len(day_range),len(y_pred)))
        print('Failed to write to csv.')
        return None
    if y_pred.shape[1]!=48:
        print('You input %d hour instead of 48!'%(y_pred.shape[1]))
        return None
    for i,vec in enumerate(y_pred):
        date = day_range[i]
        #print(date)
        tmp = pd.DataFrame(data=vec,columns=y_key)
        for hour,vec1 in tmp.iterrows():
            for st in stations:
                empty = pd.Series([date,'%s#%d'%(st,hour),vec1['%s_PM2.5'%(st)],vec1['%s_PM10'%(st)],vec1['%s_O3'%(st)]],index=['submit_date','test_id','PM2.5','PM10','O3'])
                submission = submission.append(empty,ignore_index=True)
    sample_sub = sample_sub.drop(['PM2.5','PM10','O3'],axis=1)
    pd.merge(submission,sample_sub,on=['submit_date','test_id'],how='inner').to_csv(output_csv_path,index=False)
    print('Finish write Beijing internal csv to %s.'%(output_csv_path))
    return pd.merge(submission,sample_sub,on=['submit_date','test_id'],how='inner')

def bj_pred_to_submitable_csv(y_pred,output_csv_path):
    stations = ['aotizhongx_aq', 'badaling_aq', 'beibuxinqu_aq', 'daxing_aq', 'dingling_aq', 
                'donggaocun_aq', 'dongsi_aq', 'dongsihuan_aq', 'fangshan_aq', 'fengtaihua_aq', 
                'guanyuan_aq', 'gucheng_aq', 'huairou_aq', 'liulihe_aq', 'mentougou_aq', 'miyun_aq', 
                'miyunshuik_aq', 'nansanhuan_aq', 'nongzhangu_aq', 'pingchang_aq', 'pinggu_aq', 
                'qianmen_aq', 'shunyi_aq', 'tiantan_aq', 'tongzhou_aq', 'wanliu_aq', 
                'wanshouxig_aq', 'xizhimenbe_aq', 'yanqin_aq', 'yizhuang_aq', 'yongdingme_aq', 
                'yongledian_aq', 'yufa_aq', 'yungang_aq', 'zhiwuyuan_aq']
    y_key = ['%s_%s'%(st,pol)for pol in ['PM2.5','PM10','O3'] for st in stations ]
    
    if len(y_pred)!=48:
        print('You input %d hour instead of 48!'%(y_pred.shape[1]))
        return None
    submission = pd.DataFrame([],columns = ['test_id','PM2.5','PM10','O3'])
    for hour,vec in enumerate(y_pred):
        vec = pd.Series(data=vec,index=y_key)
        for st in stations:
            empty = pd.Series(['%s#%d'%(st,hour),vec['%s_PM2.5'%(st)],vec['%s_PM10'%(st)],vec['%s_O3'%(st)]],index=['test_id','PM2.5','PM10','O3'])
            submission = submission.append(empty,ignore_index=True)
    submission.to_csv(output_csv_path,index=False)
    print('Finish write Beijing submittable csv to %s.'%(output_csv_path))
    return submission


'''
if __name__ == '__main__' and False:
    #範例
    (Xh, yh) = get_bj_hourbyhour_data(pred_features=['PM2.5','PM10','O3'],train_or_predict='train',start='2017-01-02 00:00:00',end='2018-04-24 00:00:00',training_days=1)
    (Xd, yd) = get_bj_daybyday_data(pred_features=['PM2.5','PM10','O3'],train_or_predict = 'train',start='2018-02-28',end='2018-04-19',training_days=1)
    #y_pred = y
    ''' #bj_pred_to_internal_csv(y_pred=y_pred[:,1:],output_csv_path='bj_inter.csv',sample_sub_path='/nfs/Sif/runaway/20180301_20180421_2cities_sample_submission.csv')
    #bj_pred_to_submitable_csv(y_pred=y_pred[0,1:],output_csv_path='bj_submit.csv')