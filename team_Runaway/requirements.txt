# Python 3.5.2

pandas==0.19.2
numpy==1.14.2
scikit-learn==0.19.1
tqdm
scipy
