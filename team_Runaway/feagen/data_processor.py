import os

import numpy as np
import pandas as pd
import h5py

from bistiming import SimpleTimer
from sklearn import preprocessing


Overall_norms_substring = [
    '_is_station_type', '_station_latitude', '_station_longitude']


def is_to_norm(feature):
    for f in Overall_norms_substring:
        if f in feature:
            return True
    return False


class DataProcessor(object):
    def __init__(self, config):
        self.bundle_path = os.path.join(config['feagen']['data_bundles_dir'],
                                        config['bundle']['name'] + ".h5")
        with SimpleTimer("Loading features from " + self.bundle_path,
                         end_in_new_line=False),\
             h5py.File(self.bundle_path, "r") as h5f:
            print (h5f.keys())

            self.ans = pd.read_hdf(h5f.filename, 'target')
            gases = config['bundle']['structure_config'].get('gas', list())
            if len(gases) > 0:
                self.ans = self.ans[['station', 'time'] + gases]

            norm = config['bundle']['structure_config'].get('norm', '').lower()
            norm = norm if norm in ['l1', 'l2', 'max', 'std'] else False
            # Handles normalization needed features.
            if 'features' in h5f:
                X = h5f['features'].value
                X = np.transpose(X)
                for idx, f in enumerate(
                        config['bundle']['structure']['features']):
                    # Handles normalization needed features.
                    if norm and is_to_norm(f):
                        if norm == 'std':
                            d = X[idx].reshape(-1, 1)
                            scaler = preprocessing.StandardScaler().fit(d)
                            print (f, 'mean:', scaler.mean_[0],
                                   'std:', scaler.scale_[0])
                            d = scaler.transform(d)
                            self.ans[f] = d

                        else:
                            self.ans[f] = preprocessing.normalize(
                                X[idx].reshape(1, -1), norm=norm)[0]
                    else:
                        self.ans[f] = X[idx]

    def get_subtasks(self):
        return list(self.subtask_filters.keys())

    def get_subtraining_data(self):
        return self._get_processed_data('subtraining_filters')

    def get_validation_data(self):
        return self._get_processed_data('validation_filters')

    def get_training_data(self):
        return self._get_processed_data('training_filters')

    def get_testing_data(self):
        return self._get_processed_data('testing_filters')

    def get_all_data(self):
        return self._get_processed_data()

    def _get_processed_data(self, filter_group=None):
        ans = self.ans
        site_type = self._get_filter('site_type')
        if filter_group is not None:
            filter_ = self._get_filter(filter_group)
            if filter_ is not None:
                ans = ans[filter_]
        if site_type is not None:
            ans = ans[site_type]
        # return (self.ans[filter_ & site_type])
        return ans
        # Reset the indexes for more convenient usage.
        # return (self.index_df[filter].reset_index(),
        #           self.X[filter], self.y[filter])

    def _get_filter(self, filter_group):
        filter_ = None
        with SimpleTimer('Loading filters from ' + self.bundle_path,
                         end_in_new_line=False),\
             h5py.File(self.bundle_path, 'r') as h5f:
            if filter_group not in h5f:
                return filter_
            for filter_name in h5f[filter_group]:
                data = pd.read_hdf(h5f.filename,
                                   filter_group + '/' + filter_name)
                filter_ = data if filter_ is None else (filter_ & data)
        return filter_
