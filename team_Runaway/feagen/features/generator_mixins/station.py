from feagen.decorators import (
    will_generate,
    require,
)


class StationGeneratorMixin(object):
    @require(['{city}_aq_df', '{city}_stations_df'])
    @will_generate('pandas_hdf', r'(?P<city>bj|ld)_station_latitude')
    def gen_station_latitude(self, data, will_generate_key, re_args):
        aq = data['{city}_aq_df'].value
        station_meta = data['{city}_stations_df'].value
        lats = dict(zip(station_meta.station, station_meta.latitude))
        res = aq['station'].apply(lambda st: lats[st])
        return res

    @require(['{city}_aq_df', '{city}_stations_df'])
    @will_generate('pandas_hdf', r'(?P<city>bj|ld)_station_longitude')
    def gen_station_longitude(self, data, will_generate_key, re_args):
        aq = data['{city}_aq_df'].value
        station_meta = data['{city}_stations_df'].value
        lons = dict(zip(station_meta.station, station_meta.longitude))
        res = aq['station'].apply(lambda st: lons[st])
        return res
