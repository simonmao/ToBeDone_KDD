from feagen.decorators import (
    will_generate,
    require,
)
from math import (sin, cos, pi)


class TimeGeneratorMixin(object):
    @require('{city}_aq_df')
    @will_generate('pandas_hdf', r'(?P<city>bj|ld)_time_of_day_sin')
    def gen_time_of_day_sin(self, data, will_generate_key, re_args):
        tz = 8 if re_args['city'] == 'bj' else 0
        aq = data['{city}_aq_df'].value
        # No "% 24" is needed since the divider is 24.
        hour = aq['time'].apply(lambda ts: sin(2 * pi * (ts.hour + tz) / 24))
        return hour

    @require('{city}_aq_df')
    @will_generate('pandas_hdf', r'(?P<city>bj|ld)_time_of_day_cos')
    def gen_time_of_day_cos(self, data, will_generate_key, re_args):
        tz = 8 if re_args['city'] == 'bj' else 0
        aq = data['{city}_aq_df'].value
        hour = aq['time'].apply(lambda ts: cos(2 * pi * (ts.hour + tz) / 24))
        return hour

    @require('{city}_aq_df')
    @will_generate('pandas_hdf', r'(?P<city>bj|ld)_weekday')
    def gen_weekday(self, data, will_generate_key, re_args):
        weekday = data['{city}_aq_df'].value.time.dt.weekday
        return weekday

    @require('{city}_weekday')
    @will_generate('pandas_hdf', r'(?P<city>bj|ld)_weekday_(?P<polar>cos|sin)')
    def gen_weekday_polarized(self, data, will_generate_key, re_args):
        weekday = data['{city}_weekday'].value
        polar = sin if re_args['polar'] == 'sin' else cos
        weekday = weekday.apply(lambda ts: polar(2 * pi * (ts) / 7))
        return weekday

    @require('{city}_weekday')
    @will_generate('pandas_hdf', r'(?P<city>bj|ld)_is_working_day')
    def gen_is_working_day(self, data, will_generate_key, re_args):
        weekday = data['{city}_weekday'].value
        weekday = weekday.apply(lambda ts: int(ts < 5))
        return weekday

    @require('{city}_aq_df')
    @will_generate('pandas_hdf', r'(?P<city>bj|ld)_month_(?P<polar>cos|sin)')
    def gen_month_polarized(self, data, will_generate_key, re_args):
        month = data['{city}_aq_df'].value.time.dt.month
        polar = sin if re_args['polar'] == 'sin' else cos
        month = month.apply(lambda ts: polar(2 * pi * (ts) / 12))
        return month

    @require('{city}_aq_df')
    @will_generate('pandas_hdf', r'(?P<city>bj|ld)_season_(?P<polar>cos|sin)')
    def gen_season_polarized(self, data, will_generate_key, re_args):
        # 12 => 4
        season = data['{city}_aq_df'].value.time.dt.month // 3
        polar = sin if re_args['polar'] == 'sin' else cos
        season = season.apply(lambda ts: polar(2 * pi * ts / 4))
        return season
