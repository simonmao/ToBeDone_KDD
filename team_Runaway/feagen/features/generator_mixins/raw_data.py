from feagen.decorators import (
    require,
    will_generate,
)
import pandas as pd
import tqdm
import numpy as np


def interp_stations(data, limit=None):
    stat_groups = data.groupby('station')
    res = []
    for station in data.station.unique():
        ori = stat_groups.get_group(station)
        ori = ori.interpolate('linear', limit=limit)
        res.append(ori)
    return pd.concat(res, ignore_index=True)


def nan_cnt(data):
    return data.isnull().values.sum()


def fill_nan(data, picked):
    # Part 1: Linear interpolation
    cnt = nan_cnt(data[picked])
    print ('Nan before interp: %d' % cnt)
    if cnt == 0: return data
    data = interp_stations(data, 5)
    cnt = nan_cnt(data[picked])
    print ('Nan after interp: %d' % cnt)
    if cnt == 0: return data

    # Part 2: Average fill
    dates = data.groupby('time')
    for date in tqdm.tqdm(dates.groups):  # Iter: groups by timestamp
        group = dates.get_group(date)
        for col_name in picked:
            col = group[col_name]
            nan = col.isnull()
            if nan.all():
                continue
            if nan.any():
                data.loc[col.index[nan], col_name] = col[~nan].mean()

    # Part 3: Linear interpolation
    cnt = nan_cnt(data[picked])
    print ('Nan after averaging: %d' % nan_cnt(data[picked]))
    if cnt == 0: return data

    data = interp_stations(data)
    print ('Nan after interp: %d' % nan_cnt(data[picked]))
    return data


def add_missing_datetime(df):
    hours = pd.DatetimeIndex(start=df['time'].min(),
                             end=df['time'].max(), freq="1H")
    station_groupby = df.groupby('station')
    res = []
    for station in df.station.unique():
        group = station_groupby.get_group(station)
        miss = hours[~hours.isin(group['time'])]
        new_df = pd.DataFrame()
        new_df['time'] = miss
        new_df['station'] = new_df['time'].apply(lambda x: station)
        group = group.append(new_df, ignore_index=True)
        group.sort_values('time', inplace=True)
        res.append(group)
    res = pd.concat(res, ignore_index=True)
    print (res.head(1))
    return res


def drop_if_exists(df, column_name):
    if column_name in df.columns:
        df.drop([column_name], axis=1, inplace=True)


def get_world_weather_data(aq_data, path, isbj):
    df = pd.read_csv(path, parse_dates=['time'])
    cols = ['cloudcover', 'precipMM', 'visibility',
            'pressure', 'tempC', 'windspeedKmph']
    df.rename(columns={'station_id': 'station'}, inplace=True)
    if isbj:
        df['station'] = df['station'].apply(lambda s: s[:s.find('_')])
    df = pd.merge(aq_data, df, on=['station', 'time'], how='left', sort=False)
    df = df[['station', 'time'] + cols]
    df = fill_nan(df, cols)

    print (df.head())
    return df


def get_uv_index_data(aq_data, path, normalize=False):
    df = pd.read_csv(path, parse_dates=['time'])
    if normalize:
        norm = np.linalg.norm(df['uv_index'])
        df['uv_index'] = df['uv_index'].apply(
            lambda x: x / norm if x != np.nan else np.nan)
    news = []
    for hr in range(24):
        x = df.copy()
        x['time'] = x['time'].apply(lambda t: t.replace(hour=hr))
        news.append(x)

    df = pd.concat(news, ignore_index=True)
    df = pd.merge(aq_data, df, on=['station', 'time'], how='left', sort=False)
    return df[['uv_index']].fillna(0)


def __get_neighbors(meta, data):
    """ Return (center station) => [index of 1st closest, index of 2nd ...]"""
    res = {}
    st2idx = {s: idx for idx, s in enumerate(data.station.unique())}
    # print (st2idx)

    for idx, row in meta.iterrows():
        center, lat, lon = row.station, row.latitude, row.longitude
        neighbors = {}
        for _, nrow in meta.iterrows():
            name, nlat, nlon = nrow.station, nrow.latitude, nrow.longitude
            neighbors[name] = np.square(nlon - lon) + np.square(nlat - lat)

        sorted_neighb = sorted(neighbors.items(), key=lambda x: x[1])
        res[center] = [st2idx[neighbor[0]] for neighbor in sorted_neighb[1:]]
    return res


def fill_knn_interp(meta, data, columns, k=4):
    neighbors = __get_neighbors(meta, data)

    time_group = data.groupby(['time'])
    for time, idxs in tqdm.tqdm(time_group.groups.items()):
        # if not time_group.get_group(time).isnull().values.any():
            # continue
        for r_idx in idxs:
            r = data.loc[r_idx]
            if not r.isnull().any():
                continue
            for gas in columns:
                if np.isnan(r[gas]):
                    sum, cnt = 0, 0
                    for stat_rnk in neighbors[r.station]:
                        val = data.loc[idxs[stat_rnk], gas]
                        if val != np.nan:
                            sum += val
                            cnt += 1
                        if cnt > k:
                            break
                    if cnt > 0:
                        data.loc[r_idx, gas] = sum / cnt

    print ('After knn:', nan_cnt(data))
    data = interp_stations(data)
    cnt = nan_cnt(data)
    print ('Done filling:', cnt)
    if cnt > 0: data = data.fillna(method='bfill')
    return data


class RawDataGeneratorMixin(object):
    @require('bj_stations_df')
    @will_generate('pandas_hdf', 'bj_aq_df')
    def gen_bj_aq_df(self, data, will_generate_key):
        df = pd.read_csv(self.train_data_paths['bj_aq_path'],
                         parse_dates=['utc_time'])
        df.rename(columns={'utc_time': 'time', 'stationId': 'station'},
                  inplace=True)
        df['station'] = df['station'].apply(lambda s: s[:s.find('_')])
        df.drop_duplicates(['station', 'time'], inplace=True)
        df.sort_values(['station', 'time'], inplace=True)
        df = add_missing_datetime(df)
        df = fill_knn_interp(data['bj_stations_df'].value, df,
                             'PM2.5,PM10,NO2,CO,O3,SO2'.split(','))
        # df = fill_nan(df, 'PM2.5,PM10,NO2,CO,O3,SO2'.split(','))
        return df

    @will_generate('pandas_hdf', 'bj_meo_df')
    def gen_bj_meo_df(self, will_generate_key):
        df = pd.read_csv(self.train_data_paths['bj_meo_path'],
                         parse_dates=['utc_time'])
        drop_if_exists(df, 'time')
        df.rename(columns={'utc_time': 'time', 'station_id': 'station'},
                  inplace=True)
        df['station'] = df['station'].apply(lambda s: s[:s.find('_')])
        print (df.head())
        df.drop_duplicates(['station', 'time'], inplace=True)
        df = add_missing_datetime(df)
        df.fillna(-10000, inplace=True)
        drop_if_exists(df, 'weather')
        return df

    @will_generate('pandas_hdf', 'bj_grid_df')
    def gen_bj_grid_df(self, will_generate_key):
        df = pd.read_csv(self.train_data_paths['bj_grid_path'],
                               parse_dates=['utc_time'])
        df.rename(columns={'utc_time': 'time', 'stationName': 'station',
                           'wind_speed/kph': 'wind_speed'},
                  inplace=True)
        df.drop_duplicates(['longitude', 'latitude', 'time'], inplace=True)
        df = add_missing_datetime(df)
        df = fill_nan(df, 'temperature,pressure,humidity,'
                          'wind_direction,wind_speed'.split(','))
        # longitude,latitude,time
        # temperature,pressure,humidity,wind_direction,wind_speed,weather
        drop_if_exists(df, 'weather')
        return df

    @will_generate('pandas_hdf', 'bj_stations_df')
    def gen_bj_stations_df(self, will_generate_key):
        df = pd.read_csv(self.train_data_paths['bj_station_path'])
        df.rename(columns={'station_name': 'station', 'SiteType': 'type',
                           'Latitude': 'latitude', 'Longitude': 'longitude'},
                  inplace=True)
        df['station'] = df['station'].apply(lambda s: s[:s.find('_')])
        return df

    @require('ld_stations_df')
    @will_generate('pandas_hdf', 'ld_aq_df')
    def gen_ld_aq_df(self, data, will_generate_key):
        df = pd.read_csv(self.train_data_paths['ld_aq_forecast_path'],
                         parse_dates=['MeasurementDateGMT'])
        df.rename(columns={'MeasurementDateGMT': 'time',
                           'station_id': 'station',
                           'PM2.5 (ug/m3)': 'PM2.5',
                           'PM10 (ug/m3)': 'PM10',
                           'NO2 (ug/m3)': 'NO2'},
                  inplace=True)
        # Rumor has that negative values is the power of 10.
        for gas in ['PM2.5', 'PM10', 'NO2']:
            df[gas] = df[gas].apply(lambda x: 0.1 if x < 0 else x)
        df.drop_duplicates(['station', 'time'], inplace=True)
        df.sort_values(['station', 'time'], inplace=True)
        df = add_missing_datetime(df)
        df = fill_knn_interp(data['ld_stations_df'].value, df,
                             'PM2.5,PM10,NO2'.split(','))
        # df = fill_nan(df, 'PM2.5,PM10,NO2'.split(','))
        return df

    @require('bj_aq_df')
    @will_generate('pandas_hdf', 'bj_world_weather')
    def gen_bj_world_weather(self, data, will_generate_key):
        df = data['bj_aq_df'].value
        return get_world_weather_data(
            df, self.train_data_paths['bj_worldweather'], True)  # isbj

    @require('ld_aq_df')
    @will_generate('pandas_hdf', 'ld_world_weather')
    def gen_ld_world_weather(self, data, will_generate_key):
        df = data['ld_aq_df'].value
        return get_world_weather_data(
            df, self.train_data_paths['ld_worldweather'], False)  # isbj

    @require('bj_aq_df')
    @will_generate('pandas_hdf', 'bj_uv_index')
    def gen_bj_uv_index(self, data, will_generate_key):
        df = data['bj_aq_df'].value
        return get_uv_index_data(df, self.train_data_paths['bj_uvindex'])

    @require('ld_aq_df')
    @will_generate('pandas_hdf', 'ld_uv_index')
    def gen_ld_uv_index(self, data, will_generate_key):
        df = data['ld_aq_df'].value
        return get_uv_index_data(df, self.train_data_paths['ld_uvindex'])

    @require('bj_aq_df')
    @will_generate('pandas_hdf', 'bj_uv_index_norm')
    def gen_bj_uv_index_norm(self, data, will_generate_key):
        df = data['bj_aq_df'].value
        return get_uv_index_data(df, self.train_data_paths['bj_uvindex'], True)

    @require('ld_aq_df')
    @will_generate('pandas_hdf', 'ld_uv_index_norm')
    def gen_ld_uv_index_norm(self, data, will_generate_key):
        df = data['ld_aq_df'].value
        return get_uv_index_data(df, self.train_data_paths['ld_uvindex'], True)

    @will_generate('pandas_hdf', 'ld_other_aq_df')
    def gen_ld_other_aq_df(self, will_generate_key):
        df = pd.read_csv(self.train_data_paths['ld_aq_other_path'],
                         parse_dates=['MeasurementDateGMT'])
        df.rename(columns={'MeasurementDateGMT': 'time',
                           'Station_ID': 'station',
                           'PM2.5 (ug/m3)': 'PM2.5',
                           'PM10 (ug/m3)': 'PM10',
                           'NO2 (ug/m3)': 'NO2'},
                  inplace=True)
        # Ignore all unnamed columns
        df = df.loc[:, ~df.columns.str.contains('^Unnamed')]
        return df

    @will_generate('pandas_hdf', 'ld_grid_df')
    def gen_ld_grid_df(self, will_generate_key):
        df = pd.read_csv(self.train_data_paths['ld_grid_path'],
                               parse_dates=['utc_time'])
        df.rename(columns={'utc_time': 'time', 'stationName': 'station',
                           'wind_speed/kph': 'wind_speed'},
                  inplace=True)
        df.drop_duplicates(['station', 'time'], inplace=True)
        df = add_missing_datetime(df)
        df = fill_nan(df, 'temperature,pressure,humidity,'
                          'wind_direction,wind_speed'.split(','))
        drop_if_exists(df, 'weather')
        # longitude,latitude,time
        # temperature,pressure,humidity,wind_direction,wind_speed,weather
        return df

    @will_generate('pandas_hdf', 'ld_stations_df')
    def gen_ld_stations_df(self, will_generate_key):
        df = pd.read_csv(self.train_data_paths['ld_station_path'])
        df.rename(columns={'Unnamed: 0': 'station', 'SiteType': 'type',
                           'Latitude': 'latitude', 'Longitude': 'longitude'},
                  inplace=True)
        bool_cols = ['need_prediction', 'api_data', 'historical_data']
        df[bool_cols] = df[bool_cols].fillna(False)
        df.drop(['SiteName'], axis=1, inplace=True)
        df = df[df['need_prediction'] == True]
        # station,api_data,need_prediction,historical_data,
        # latitude,longitude,type
        return df
