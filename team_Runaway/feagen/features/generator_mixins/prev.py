from feagen.decorators import (
    will_generate,
    require,
)
import pandas as pd

GAS = 'PM2\.5|PM10|O3|NO2|CO|SO2'


class PrevDataGeneratorMixin(object):
    @require('{city}_aq_df')
    @will_generate('pandas_hdf',
                   r'(?P<city>bj|ld)_prev_(?P<n>[0-9]+)_' +
                   r'(?P<time>hour|day|week|month|year)_(?P<col>' + GAS + ')')
    def gen_prev_data(self, data, will_generate_key, re_args):
        aq = data['{city}_aq_df'].value
        target = re_args['col']
        offset = {re_args['time'] + 's': int(re_args['n'])}
        aq['shifted'] = pd.DatetimeIndex(aq['time']) + pd.DateOffset(**offset)
        ans = pd.merge(aq[['station', 'time']],
                       aq[['station', 'shifted', target]],
                       left_on=['station', 'time'],
                       right_on=['station', 'shifted'],
                       how='left', sort=False)
        ans.fillna(0, inplace=True)
        print ('Offset:', ans.head())

        return ans[target]
