from feagen.decorators import (
    will_generate,
    require,
)
import numpy as np
import pandas as pd

CITY_SITES = {
    'bj': ['Urban', 'Suburban', 'Other', 'Roadside'],
    'ld': ['Urban Background', 'Suburban', 'Roadside',
           'Kerbside', 'Industrial']
}


def ohot(length, idx):
    res = np.zeros(length)
    res[idx] = 1
    return res


class TypeGeneratorMixin(object):
    @require(['{city}_aq_df', '{city}_stations_df'])
    @will_generate('pandas_hdf', r'(?P<city>bj|ld)_site_type(?P<type>[1-5])')
    def gen_site_type_filter(self, data, will_generate_key, re_args):
        stations = data['{city}_aq_df'].value['station']
        typ = CITY_SITES[re_args['city']][int(re_args['type']) - 1]
        stat_meta = data['{city}_stations_df'].value
        selected = stat_meta.loc[stat_meta['type'] == typ, 'station']
        return stations.isin(selected)

    @require(['{city}_aq_df', '{city}_stations_df'])
    @will_generate('pandas_hdf',
                   r'(?P<city>bj|ld)_is_station_type(?P<type>[1-5])')
    def gen_station_type_onehot(self, data, will_generate_key, re_args):
        aq = data['{city}_aq_df'].value
        station_meta = data['{city}_stations_df'].value
        res = pd.DataFrame()
        typ = CITY_SITES[re_args['city']][int(re_args['type']) - 1]
        stations = set(station_meta[station_meta['type'] == typ]['station'])
        res['station_type_' + typ] = aq['station'].apply(
            lambda st: int(st in stations))
        print (res.head())
        print (res.shape)
        return res
