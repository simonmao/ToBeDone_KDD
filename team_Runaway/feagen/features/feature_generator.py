import feagen
import pandas as pd

from .generator_mixins import (
    RawDataGeneratorMixin,
    GridGeneratorMixin,
    FilterGeneratorMixin,
    TypeGeneratorMixin,
    TimeGeneratorMixin,
    StationGeneratorMixin,
    PrevDataGeneratorMixin,
)


width, _ = pd.util.terminal.get_terminal_size()
pd.set_option('display.width', width)


class KDDCup2018FeatureGenerator(RawDataGeneratorMixin,
                                 GridGeneratorMixin,
                                 FilterGeneratorMixin,
                                 TypeGeneratorMixin,
                                 TimeGeneratorMixin,
                                 StationGeneratorMixin,
                                 PrevDataGeneratorMixin,
                                 feagen.FeatureGenerator):
    def __init__(self, h5py_hdf_path, pandas_hdf_path, pickle_dir,
                 train_data_paths, test_data_paths):
        super(KDDCup2018FeatureGenerator, self).__init__(
            h5py_hdf_path=h5py_hdf_path,
            pandas_hdf_path=pandas_hdf_path,
            pickle_dir=pickle_dir)
        self.train_data_paths = train_data_paths
        self.test_data_paths = test_data_paths
