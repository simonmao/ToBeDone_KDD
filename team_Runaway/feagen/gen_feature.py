# coding: utf-8

from serialtime import try_load_yaml
import pprint
import os
import sys

if len(sys.argv) < 3:
    print ('Usage: %s [train/predict] [bj/ld] [bundle_file]' % sys.argv[0])
    sys.exit(1)

typ = sys.argv[1]
city = sys.argv[2]
idx = sys.argv[3]

FEAGEN_CONF = '/nfs/Sif/runaway/Runaway/kdd/.feagenrc/config_%s.yml' % typ
feagen_config = try_load_yaml(FEAGEN_CONF)
pprint.pprint(feagen_config, depth=2)

#BUNDLE_CONF = '.feagenrc/bundle_config_example.yml'
BUNDLE_CONF = '/nfs/Sif/runaway/Runaway/kdd/.feagenrc/bundle_%s_config_%s.yml' % (city, idx)

bundle_config = try_load_yaml(BUNDLE_CONF)
pprint.pprint(bundle_config)

config = {
    'feagen': feagen_config,
    'bundle': bundle_config,
}

from data_processor import DataProcessor

dp = DataProcessor(config)

X = dp.get_all_data()
output = '/nfs/Sif/runaway/kdd_data/Mao/up_to_date_data/%s_%s_%s.csv' % (city, typ, idx)
#os.remove(output)
X.to_csv(output, index=False)
#os.chmod(output, 0o770)

print ('\nX:', X.shape)
print (X.head(2), '\n', X.tail(2))
