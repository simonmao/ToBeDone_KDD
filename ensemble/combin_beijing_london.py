import pandas as pd
import numpy as np

import sys

beijing_panda = pd.read_csv(sys.argv[1])
london_panda = pd.read_csv(sys.argv[2])

beijing_panda['hour'] = [int(test_id.split('#')[1]) for test_id in beijing_panda['test_id']]
london_panda['hour'] = [int(test_id.split('#')[1]) for test_id in london_panda['test_id']]
beijing_panda['stationid'] = [test_id.split('#')[0] for test_id in beijing_panda['test_id']]
london_panda['stationid'] = [test_id.split('#')[0] for test_id in london_panda['test_id']]


station_list = ['CD1', 'BL0', 'GR4', 'MY7', 'HV1', 'GN3', 'GR9', 'LW2', 'GN0', 'KF1', 'CD9', 'ST5', 'TH4']

beijing_panda = beijing_panda[~beijing_panda['stationid'].isin(station_list)]
london_panda = london_panda[london_panda['stationid'].isin(station_list)]

final_panda = pd.concat([beijing_panda, london_panda]).sort_values(by=['stationid', 'hour'])
final_panda.to_csv(sys.argv[3], index=False, columns=['submit_date', 'test_id', 'PM2.5', 'PM10', 'O3'])
